pipeline {
    agent {label 'physix_agent'}
    environment {
        // GRAFFY_OWNCLOUD_JENKINS_PWD's value has been obtained from https://cloud.ipr.univ-rennes1.fr as an app password and is defined in gitlab as a CI secret variable,
        // then this app password has been declared to jenkins's credentials as a secret text with the name graffy-cloudipr-for-jenkinsipr
        GRAFFY_OWNCLOUD_JENKINS_PWD = credentials('graffy-cloudipr-for-jenkinsipr')
        // not sure pwd is the same as jenkin's variable $WORKSPACE
        BUILD_ROOT_PATH = '${PWD}/build'  
    }
    stages {
        stage('Initial setup...') {
            steps {
                echo 'Initial setup...'
                // erase old builds if anything is left
                sh './scripts/cleanup.bash'
                sh './scripts/install_fiji.bash'
                sh 'make BUILD_ROOT_PATH=$BUILD_ROOT_PATH FIJI_ROOT_PATH=$(pwd)/Fiji.app install'
            }
        }
        stage('Testing the package...') {
            steps {
                sh 'make BUILD_ROOT_PATH=$BUILD_ROOT_PATH FIJI_ROOT_PATH=$(pwd)/Fiji.app RAW_IMAGES_ROOT_PATH=/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase/raw-images TESTS_OUTPUT_DATA_PATH=$TMP/$(whoami)/$JOB_NAME test'
            }
        }
        stage('Building documentation...') {
            steps {
                echo 'Building documentation...'
                sh 'make BUILD_ROOT_PATH=$BUILD_ROOT_PATH FIJI_ROOT_PATH=$(pwd)/Fiji.app RAW_IMAGES_ROOT_PATH=/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase/raw-images doc'
            }
        }
        stage('Releasing package...') {
            steps {
                echo 'Releasing package...'
                sh './scripts/release_lipase_package.bash'
            }
        }
        stage('Cleaning up...') {
            steps {
                echo 'Cleaning artifacts...'
                sh './scripts/cleanup.bash'
                // sh 'rm -rf ./install_resources'
                // sh 'cd ./src/doc && make clean'
            }
        }
    }
    post
    {
        // always, success, failure, unstable, changed
        failure
        {
            mail bcc: '', body: "<b>Validation failed</b><br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br>Build URL: ${env.BUILD_URL}", cc: '', charset: 'UTF-8', from: '', mimeType: 'text/html', replyTo: '', subject: "CI build failed for ${env.JOB_NAME}", to: "guillaume.raffy@univ-rennes1.fr";
        }
    }
}
