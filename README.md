# lipase

An image processing software suite for the project lipase (Véronique Vié, Institut de Physique de Rennes)

This software suite is developed as a python script for fiji (an image processing package based on imageJ).

## What it does

As the development is at its early stages, it does not much at the moment:

- loads a sequence of images into fiji in the form of an hyperstack (5d images : width, height, depth, channels, time)

## How to install

1. make sure that fiji is installed on your computer. If not, get it from https://fiji.sc/
2. make sure that fiji has the `IJ-OpenCV-plugins`
    - in Fiji, select the `Help->Update...` menu item
    - in the **ImageJ Updater** window, click on the **Manage update sites** button. This opens the **Manage update sites** window.
    - in the **Manage update sites** window, tick the `IJ-OpenCV-plugins` (URL = http://sites.imagej.net/IJ-OpenCV/) if it's not ticked.
    - click the `close` button of the **Manage update sites** window
    - click `Apply changes` button in the **ImageJ Updater** window
    - wait until the plugins are installed and quit Fiji
3. Install lipase
    - lipase is released as a package named `lipase-<lipase-version>.zip`. This package can be built from sources using the `make package` command, provided you have all the build tools on your machine (make, javac, zip).
    - simply unzip the package `lipase-<lipase-version>.zip` in Fiji root directory (the directory that contains Fiji executable eg /home/graffy/soft/Fiji.app)
4. launch fiji and check that you have lipase tools in the menu `Plugins/Ipr/Lipase`

## How to use

see `lipase-manual.pdf` (this document can be built from sources using the `make doc` command, provided you have all the build tools on your machine (make, pdflatex, imagemagick)

## Notes

http://imagejdocu.tudor.lu/doku.php?id=plugin:analysis:microscope_image_correlation_spectroscopy:start
