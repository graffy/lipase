#@ ImagePlus (label="the input image") INPUT_IMAGE
#@ Float (label="maximal radius", value=32.0, min=0.0, max=100.0, style="slider") MAX_RADIUS
#@ Integer (label="number of radial sectors", value=8, min=0, max=100, style="slider") NUM_RADIAL_SECTORS
#@ Integer (label="number of angular sectors", value=4, min=0, max=100, style="slider") NUM_ANGULAR_SECTORS

#@output ImagePlus RADIAL_PROFILES
#@output ImagePlus ANGULAR_STDDEV_PROFILES
"""This script is supposed to be launched from fiji's jython interpreter

This imagej plugin computes the radial profile of each pixel of the input image. The resulting profiles are stored in a single hyperstack, where the profile data are stored along the channel axis
"""

# # note: fiji's jython doesn't support encoding keyword

from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

from lipase.imageengine import IImageEngine, PixelType
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.circsymdetector import CircularSymmetryDetector

import ij.gui  # pylint: disable=import-error
from jarray import zeros, array  # pylint: disable=import-error

def run_script():
    global INPUT_IMAGE  # pylint:disable=global-variable-not-assigned
    global MAX_RADIUS  # pylint:disable=global-variable-not-assigned
    global NUM_RADIAL_SECTORS  # pylint:disable=global-variable-not-assigned
    global NUM_ANGULAR_SECTORS  # pylint:disable=global-variable-not-assigned
    global RADIAL_PROFILES  # pylint:disable=global-variable-not-assigned
    global ANGULAR_STDDEV_PROFILES  # pylint:disable=global-variable-not-assigned
    IImageEngine.set_instance(IJImageEngine())

    src_image = IImageEngine.get_instance().create_image(width=1, height=1, pixel_type=PixelType.U8)
    src_image.ij_image = INPUT_IMAGE  # pylint: disable=undefined-variable

    detector = CircularSymmetryDetector(max_radius=MAX_RADIUS, num_angular_sectors=NUM_ANGULAR_SECTORS, num_radial_sectors=NUM_RADIAL_SECTORS)  # pylint: disable=undefined-variable
    radial_profiles, angular_stddev_profiles = detector.compute_radial_profiles(src_image)

    RADIAL_PROFILES = radial_profiles.hyperstack
    ANGULAR_STDDEV_PROFILES = angular_stddev_profiles.hyperstack


# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
