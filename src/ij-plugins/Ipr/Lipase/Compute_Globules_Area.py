#@ ImagePlus (label="the input image stack") INPUT_STACK
#@ ImagePlus (label="the background image") INPUT_BACKGROUND
#@ Integer (label="particle threshold (in gray levels)", value=2000, min=0, max=32768, style="slider") PARTICLE_THRESHOLD

#@output ImagePlus OUTPUT_PLOT
"""This script is supposed to be launched from fiji's jython interpreter
This plugin estimates the global area of globules
"""


# # note: fiji's jython doesn't support encoding keyword

from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

# from lipase import Lipase, ImageLogger
from lipase.imageengine import IImageEngine, PixelType, StackImageFeeder, IImageProcessingDebugger
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.lipase import UserProvidedBackground, GlobulesAreaEstimator
# from lipase.improc.improlis import IMovieProcessListener
# from ij import IJ  # pylint: disable=import-error
# from ij.gui import GenericDialog, DialogListener # pylint: disable=import-error
# from java.awt.event import ItemListener # pylint: disable=import-error
import ij.gui
from jarray import zeros, array


class IsParticleCollector(IImageProcessingDebugger):

    def __init__(self, num_frames):
        IImageProcessingDebugger.__init__(self)
        ie = IImageEngine.get_instance()
        self.is_particle = None
        self.num_frames = num_frames
        self.frame_index = 0

    def on_image(self, image, image_id):
        # print('IsParticleCollector.on_image : image_id = %s' % image_id)
        if image_id == 'is_particle':
            ie = IImageEngine.get_instance()
            if self.is_particle is None:
                self.is_particle = ie.create_hyperstack(width=image.get_width(), height=image.get_height(), num_channels=1, num_slices=1, num_frames=self.num_frames, pixel_type=PixelType.U8)
                self.is_particle.set_image(image=image, frame_index=self.frame_index)
            print("IsParticleCollector.on_image : %f %f" % image.get_value_range())
            self.is_particle.set_image(image=image, frame_index=self.frame_index)
            self.frame_index += 1

    def on_hyperstack(self, hyperstack, hyperstack_id):
        # print('IsParticleCollector.on_hyperstack : hyperstack_id = %s' % hyperstack_id)
        pass

def run_script():
    global INPUT_STACK  # pylint:disable=global-variable-not-assigned
    global INPUT_BACKGROUND  # pylint:disable=global-variable-not-assigned
    global PARTICLE_THRESHOLD  # pylint:disable=global-variable-not-assigned
    global OUTPUT_PLOT  # pylint:disable=global-variable-not-assigned

    IImageEngine.set_instance(IJImageEngine())

    src_hyperstack = IImageEngine.get_instance().create_hyperstack(width=1, height=1, num_channels=1, num_slices=1, num_frames=1, pixel_type=PixelType.U8)
    src_hyperstack.hyperstack = INPUT_STACK  # pylint: disable=undefined-variable

    src_background = IImageEngine.get_instance().create_image(width=1, height=1, pixel_type=PixelType.U8)
    src_background.ij_image = INPUT_BACKGROUND  # pylint: disable=undefined-variable

    background_estimator = UserProvidedBackground(background_image=src_background)
    processor = GlobulesAreaEstimator(background_estimator=background_estimator, particle_threshold=PARTICLE_THRESHOLD)  # pylint: disable=undefined-variable
    is_particle_collector = IsParticleCollector(num_frames = src_hyperstack.num_frames())
    results = processor.detect_particles(src_hyperstack, image_proc_debugger=is_particle_collector)
    # save_hdf5_file('results.h5', results)
    # results file could be checked with "h5dump --xml ./lipase.git/results.h5"


    x = array(results['frame index'].elements, 'f')
    y = array(results['globules_area_ratio'].elements, 'f')

    plot = ij.gui.Plot('globules area graph', 'frame', 'globules area ratio', x, y)
    plot.show()

    print("Compute_Globules_Area is_particle_collector.is_particle size : %d" % (is_particle_collector.is_particle.num_frames()))
    first_frame = is_particle_collector.is_particle.get_image(frame_index=0, slice_index=0, channel_index=0)
    print("Compute_Globules_Area is_particle_collector.is_particle size : %f %f" % first_frame.get_value_range())

    is_particle_collector.is_particle.hyperstack.setTitle('is_particle')
    is_particle_collector.is_particle.hyperstack.show()

    OUTPUT_PLOT = plot


# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
