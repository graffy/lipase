#@ ImagePlus (label="the input image") INPUT_IMAGE
#@ ImagePlus (label="mask containing non-zero values for pixels that should be ignored") MASK_IMAGE
#@ Float (label="maximal radius of globules", value=32.0, min=0.0, max=100.0, style="slider") MAX_RADIUS
#@ Integer (label="number of radial sectors", value=8, min=0, max=100, style="slider") NUM_RADIAL_SECTORS
#@ Integer (label="number of angular sectors", value=4, min=0, max=100, style="slider") NUM_ANGULAR_SECTORS
#@ Float (label="max finder threshold", value=200.0) MAX_FINDER_THRESHOLD
#@ Float (label="max finder tolerance", value=1000.0) MAX_FINDER_TOLERANCE

"""This script is supposed to be launched from fiji's jython interpreter

This imagej plugin detects circular shaped particules in the input image, using a filter that computes the "circularness" of pixels. The circularness of a pixel is defined by its likeliness of being the center of a circular symmetry.
"""

# # note: fiji's jython doesn't support encoding keyword

from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

from lipase.imageengine import IImageEngine, PixelType
from lipase.maxima_finder import MaximaFinder
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.circsymdetector import CircularSymmetryDetector, GlobulesDetector

import ij.gui  # pylint: disable=import-error
from jarray import zeros, array  # pylint: disable=import-error

def run_script():
    global INPUT_IMAGE  # pylint:disable=global-variable-not-assigned
    global MASK_IMAGE  # pylint:disable=global-variable-not-assigned
    global MAX_RADIUS  # pylint:disable=global-variable-not-assigned
    global NUM_RADIAL_SECTORS  # pylint:disable=global-variable-not-assigned
    global NUM_ANGULAR_SECTORS  # pylint:disable=global-variable-not-assigned
    global MAX_FINDER_THRESHOLD  # pylint:disable=global-variable-not-assigned
    global MAX_FINDER_TOLERANCE  # pylint:disable=global-variable-not-assigned
    IImageEngine.set_instance(IJImageEngine())
    ie = IImageEngine.get_instance()
    src_image = ie.create_image(width=1, height=1, pixel_type=PixelType.U8)
    src_image.ij_image = INPUT_IMAGE  # pylint: disable=undefined-variable

    mask_image = None
    if MASK_IMAGE:  # pylint: disable=undefined-variable
        mask_image = ie.create_image(width=1, height=1, pixel_type=PixelType.U8)
        mask_image.ij_image = MASK_IMAGE  # pylint: disable=undefined-variable

    # globules_edges = ie.compute_edge_transform(globules_image)
    circ_sym_filter = CircularSymmetryDetector(max_radius=MAX_RADIUS, num_angular_sectors=NUM_ANGULAR_SECTORS, num_radial_sectors=NUM_RADIAL_SECTORS)  # pylint: disable=undefined-variable
    circles_finder = MaximaFinder(threshold=MAX_FINDER_THRESHOLD, tolerance=MAX_FINDER_TOLERANCE)  # pylint: disable=undefined-variable
    detector = GlobulesDetector(circ_sym_filter=circ_sym_filter, circles_finder=circles_finder, ignore_mask=mask_image)
    detected_globules = detector.detect_globules(src_image)
    
    points_roi = ij.gui.PointRoi()
    # points_roi.addPoint(21, 17)
    # points_roi.addPoint(13, 34)
     
    for globule in detected_globules:
        points_roi.addPoint(globule.x, globule.y)

    src_image.ij_image.setRoi(points_roi)

# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
