#@ String (choices={"no smooth", "gaussian blur"}, style="listBox", value="gaussian blur") DARK_SMOOTH_METHOD
#@ Float (label="dark image gaussian blur sigma", style="slider", min=0.0, max=10.0, stepSize=0.1, value=3.0) DARK_GAUSSIAN_BLUR_SIGMA
#@ ImagePlus (label="the input image stack") INPUT_STACK
#@ ImagePlus (label="the input white image") INPUT_WHITE
#@ ImagePlus (label="the input dark image") INPUT_DARK
#@output ImagePlus PREPROCESSED_STACK

# failed attempt to use alternative input parameters for headless testing but visibility=INVISIBLE doesn't make their widget disappear, unfortunately (see https://imagej.net/Script_parameters.html#Default_values)
## String (label="the input image stack", visibility=INVISIBLE) INPUT_STACK_FILE
## String (label="the input white image", visibility=INVISIBLE) INPUT_WHITE_FILE
## String (label="the input dark image", visibility=INVISIBLE) INPUT_DARK_FILE

"""This script is supposed to be launched from fiji's jython interpreter
"""

# to use log.info (see 3d_analytics_CH.py sample)
#@LogService log


#from ij import IJ  # pylint: disable=import-error

#WHITE_ESTIMATE = IJ.openImage('/Users/graffy/ownCloud/ipr/lipase/lipase.git/white_estimate.tiff')

# # note: fiji's jython doesn't support encoding keyword

# https://imagej.net/Scripting_Headless
# String lipase_src_root_path

# String(label="Please enter your name",description="Name field") name
# OUTPUT String greeting
from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

# it is necassary to add lipase's root path to the path if run from fiji as script otherwise jython fails to find lipase's modules such as catalog
# sys.path.append(lipase_src_root_path)  # pylint: disable=undefined-variable

# from lipase import Lipase, ImageLogger
from lipase.imageengine import IImageEngine, PixelType, StackImageFeeder
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.telemos import WhiteEstimator
from lipase.catalog import ImageCatalog

from ij import IJ
from ij.gui import GenericDialog, DialogListener
from ij.plugin.filter import GaussianBlur
from java.awt.event import ItemListener


def run_script():
    user_settings = UserSettings()
    ie = IJImageEngine()
    IImageEngine.set_instance(ie)
    catalog = ImageCatalog(user_settings.raw_images_root_path)

    src_hyperstack = IImageEngine.get_instance().create_hyperstack(width=1, height=1, num_channels=1, num_slices=1, num_frames=1, pixel_type=PixelType.U8)
    src_hyperstack.hyperstack = INPUT_STACK

    src_white = IImageEngine.get_instance().create_image(width=1, height=1, pixel_type=PixelType.F32)
    src_white.ij_image = INPUT_WHITE
    if src_white.get_pixel_type() != PixelType.F32:
        src_white = src_white.clone(clone_pixel_type=PixelType.F32)

    src_dark = IImageEngine.get_instance().create_image(width=1, height=1, pixel_type=PixelType.F32)
    src_dark.ij_image = INPUT_DARK
    if src_dark.get_pixel_type() != PixelType.F32:
        src_dark = src_dark.clone(clone_pixel_type=PixelType.F32)

    global DARK_SMOOTH_METHOD
    global DARK_GAUSSIAN_BLUR_SIGMA
    log.info('DARK_SMOOTH_METHOD=%s' % DARK_SMOOTH_METHOD)
    if DARK_SMOOTH_METHOD == 'gaussian blur':
        log.info('smoothing dark image with gaussian blur filter (sigma=%f)' % DARK_GAUSSIAN_BLUR_SIGMA)
        # ij.plugin.filter.GaussianBlur
        # blurGaussian
        gaussian_blur = GaussianBlur()
        accuracy = 0.0002  # comes from https://imagej.nih.gov/ij/developer/source/ij/plugin/filter/GaussianBlur.java.html
        gaussian_blur.blurFloat(src_dark.ij_image.getProcessor(), DARK_GAUSSIAN_BLUR_SIGMA, DARK_GAUSSIAN_BLUR_SIGMA, accuracy)

    dst_preproc_stack = IImageEngine.get_instance().create_hyperstack(width=src_hyperstack.get_width(), height=src_hyperstack.get_height(), num_channels=src_hyperstack.num_channels(), num_slices=src_hyperstack.num_slices(), num_frames=src_hyperstack.num_frames(), pixel_type=PixelType.F32)

    src_image_feeder = StackImageFeeder(src_hyperstack)

    src_it = iter(src_image_feeder)
    frame_index = 0
    for src_image in src_it:
        nomin_image = ie.subtract(src_image, src_dark)
        denom_image = ie.subtract(src_white, src_dark)
        preproc_image = ie.divide(nomin_image, denom_image)
        dst_preproc_stack.set_image(preproc_image, frame_index=frame_index, slice_index=0, channel_index=0)  # TODO: use frame_index, slice_index and channel_index from feeder
        frame_index += 1

    global PREPROCESSED_STACK
    PREPROCESSED_STACK = dst_preproc_stack.hyperstack

# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
