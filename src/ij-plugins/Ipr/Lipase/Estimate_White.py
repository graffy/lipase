#@ ImagePlus (label="the input image stack") INPUT_STACK
#@ Integer (label="open size (in pixels)", value=75, min=0, max=500, style="slider") OPEN_SIZE
#@ Integer (label="close size (in pixels)", value=75, min=0, max=500, style="slider") CLOSE_SIZE
#@ Integer (label="average size (in pixels)", value=75, min=0, max=500, style="slider") AVERAGE_SIZE

#@output ImagePlus WHITE_ESTIMATE
"""This script is supposed to be launched from fiji's jython interpreter
"""

#from ij import IJ  # pylint: disable=import-error

#WHITE_ESTIMATE = IJ.openImage('/Users/graffy/ownCloud/ipr/lipase/lipase.git/white_estimate.tiff')

# # note: fiji's jython doesn't support encoding keyword

# https://imagej.net/Scripting_Headless
# String lipase_src_root_path

# String(label="Please enter your name",description="Name field") name
# OUTPUT String greeting
from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

# it is necassary to add lipase's root path to the path if run from fiji as script otherwise jython fails to find lipase's modules such as catalog
# sys.path.append(lipase_src_root_path)  # pylint: disable=undefined-variable

# from lipase import Lipase, ImageLogger
from lipase.imageengine import IImageEngine, PixelType, StackImageFeeder
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.telemos import WhiteEstimator
from lipase.catalog import ImageCatalog

from ij import IJ
from ij.gui import GenericDialog, DialogListener
from java.awt.event import ItemListener

# class MyListener(DialogListener):
#     def dialogItemChanged(self, gd, event):
#         IJ.log("Something was changed (event = %s)" % event)
#         IJ.log("event's attributes : %s" % str(dir(event)))
        
#         # Something was changed (event = java.awt.event.ItemEvent[ITEM_STATE_CHANGED,item=res_soleil2018/DARK/DARK_40X_60min_1 im pae min_1/Pos0,stateChange=SELECTED] on choice0)

#         # event's attributes : ['ACTION_EVENT_MASK', 'ADJUSTMENT_EVENT_MASK', 'COMPONENT_EVENT_MASK', 'CONTAINER_EVENT_MASK', 'DESELECTED', 'FOCUS_EVENT_MASK', 'HIERARCHY_BOUNDS_EVENT_MASK', 'HIERARCHY_EVENT_MASK', 'ID', 'INPUT_METHOD_EVENT_MASK', 'INVOCATION_EVENT_MASK', 'ITEM_EVENT_MASK', 'ITEM_FIRST', 'ITEM_LAST', 'ITEM_STATE_CHANGED', 'KEY_EVENT_MASK', 'MOUSE_EVENT_MASK', 'MOUSE_MOTION_EVENT_MASK', 'MOUSE_WHEEL_EVENT_MASK', 'PAINT_EVENT_MASK', 'RESERVED_ID_MAX', 'SELECTED', 'TEXT_EVENT_MASK', 'WINDOW_EVENT_MASK', 'WINDOW_FOCUS_EVENT_MASK', 'WINDOW_STATE_EVENT_MASK', '__class__', '__copy__', '__deepcopy__', '__delattr__', '__doc__', '__ensure_finalizer__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__str__', '__subclasshook__', '__unicode__', 'class', 'equals', 'getClass', 'getID', 'getItem', 'getItemSelectable', 'getSource', 'getStateChange', 'hashCode', 'item', 'itemSelectable', 'notify', 'notifyAll', 'paramString', 'setSource', 'source', 'stateChange', 'toString', 'wait']


def run_script():
    user_settings = UserSettings()
    IImageEngine.set_instance(IJImageEngine())
    catalog = ImageCatalog(user_settings.raw_images_root_path)
    src_hyperstack = IImageEngine.get_instance().create_hyperstack(width=1, height=1, num_channels=1, num_slices=1, num_frames=1, pixel_type=PixelType.U8)
    src_hyperstack.hyperstack = INPUT_STACK
    image_set = StackImageFeeder(src_hyperstack)
    white_estimator = WhiteEstimator(open_size=OPEN_SIZE, close_size=CLOSE_SIZE, average_size=AVERAGE_SIZE)
    white_estimate = white_estimator.estimate_white_from_image_set(image_set)
    print(type(white_estimate))
    global WHITE_ESTIMATE
    WHITE_ESTIMATE = white_estimate.ij_image

# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
