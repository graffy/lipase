#@ File (label="Select the root directory of root images", style="directory") RAW_IMAGES_ROOT_PATH

# https://imagej.net/Script_Parameters
# @ output String INFO

# @File(label = "Image File", persist=True) FILENAME
"""This script is supposed to be launched from fiji's jython interpreter
"""
# # note: fiji's jython doesn't support encoding keyword

# https://imagej.net/Scripting_Headless

from lipase.settings import UserSettings

# print("RAW_IMAGES_ROOT_PATH=%s", RAW_IMAGES_ROOT_PATH)

# it is necassary to add lipase's root path to the path if run from fiji as script otherwise jython fails to find lipase's modules such as catalog
# sys.path.append(lipase_src_root_path)  # pylint: disable=undefined-variable

def run_script():
    settings = UserSettings()
    settings.raw_images_root_path = unicode(RAW_IMAGES_ROOT_PATH)
    settings.save()
    # global INFO
    # INFO = "the root directory containing raw images (%s) has been stored in your settings file %s" % (str(RAW_IMAGES_ROOT_PATH), settings.get_settings_file_path())
# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
