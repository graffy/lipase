
#@output ImagePlus SEQUENCE
"""This script is supposed to be launched from fiji's jython interpreter
"""

#from ij import IJ  # pylint: disable=import-error

#WHITE_ESTIMATE = IJ.openImage('/Users/graffy/ownCloud/ipr/lipase/lipase.git/white_estimate.tiff')

# # note: fiji's jython doesn't support encoding keyword

# https://imagej.net/Scripting_Headless
# String lipase_src_root_path

# String(label="Please enter your name",description="Name field") name
# OUTPUT String greeting
from lipase import logger
import sys
logger.debug('python version %s' % sys.version)

from lipase.settings import UserSettings

# it is necassary to add lipase's root path to the path if run from fiji as script otherwise jython fails to find lipase's modules such as catalog
# sys.path.append(lipase_src_root_path)  # pylint: disable=undefined-variable

# from lipase import Lipase, ImageLogger
from lipase.imageengine import IImageEngine
from lipase.imagej.ijimageengine import IJImageEngine
from lipase.telemos import WhiteEstimator
from lipase.catalog import ImageCatalog
from lipase.imagej import open_sequence_in_imagej
from lipase import logger

from ij import IJ
from ij.gui import GenericDialog, DialogListener
from java.awt.event import ItemListener

# class MyListener(DialogListener):
#     def dialogItemChanged(self, gd, event):
#         IJ.log("Something was changed (event = %s)" % event)
#         IJ.log("event's attributes : %s" % str(dir(event)))
        
#         # Something was changed (event = java.awt.event.ItemEvent[ITEM_STATE_CHANGED,item=res_soleil2018/DARK/DARK_40X_60min_1 im pae min_1/Pos0,stateChange=SELECTED] on choice0)

#         # event's attributes : ['ACTION_EVENT_MASK', 'ADJUSTMENT_EVENT_MASK', 'COMPONENT_EVENT_MASK', 'CONTAINER_EVENT_MASK', 'DESELECTED', 'FOCUS_EVENT_MASK', 'HIERARCHY_BOUNDS_EVENT_MASK', 'HIERARCHY_EVENT_MASK', 'ID', 'INPUT_METHOD_EVENT_MASK', 'INVOCATION_EVENT_MASK', 'ITEM_EVENT_MASK', 'ITEM_FIRST', 'ITEM_LAST', 'ITEM_STATE_CHANGED', 'KEY_EVENT_MASK', 'MOUSE_EVENT_MASK', 'MOUSE_MOTION_EVENT_MASK', 'MOUSE_WHEEL_EVENT_MASK', 'PAINT_EVENT_MASK', 'RESERVED_ID_MAX', 'SELECTED', 'TEXT_EVENT_MASK', 'WINDOW_EVENT_MASK', 'WINDOW_FOCUS_EVENT_MASK', 'WINDOW_STATE_EVENT_MASK', '__class__', '__copy__', '__deepcopy__', '__delattr__', '__doc__', '__ensure_finalizer__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__str__', '__subclasshook__', '__unicode__', 'class', 'equals', 'getClass', 'getID', 'getItem', 'getItemSelectable', 'getSource', 'getStateChange', 'hashCode', 'item', 'itemSelectable', 'notify', 'notifyAll', 'paramString', 'setSource', 'source', 'stateChange', 'toString', 'wait']


class SequenceChoiceListener(ItemListener):

    def __init__(self, channel_choice, catalog):
        """
        Args:
                channel_choice (java.awt.Choice): The widget used for choosing the channel
                catalog (Catalog): The catalog of sequences
        """
        ItemListener.__init__(self)
        self.channel_choice = channel_choice
        self.catalog = catalog

    def itemStateChanged(self, event):
        logger.debug("SequenceChoiceListener : Something was changed (event = %s)" % event)
        # SequenceChoiceListener : Something was changed (event = java.awt.event.ItemEvent[ITEM_STATE_CHANGED,item=res_soleil2018/DARK/DARK_40X_60min_1 im pae min_1/Pos0,stateChange=SELECTED] on choice3)
        logger.debug("SequenceChoiceListener : event's attributes : %s" % str(dir(event)))
        # SequenceChoiceListener : event's attributes : ['ACTION_EVENT_MASK', 'ADJUSTMENT_EVENT_MASK', 'COMPONENT_EVENT_MASK', 'CONTAINER_EVENT_MASK', 'DESELECTED', 'FOCUS_EVENT_MASK', 'HIERARCHY_BOUNDS_EVENT_MASK', 'HIERARCHY_EVENT_MASK', 'ID', 'INPUT_METHOD_EVENT_MASK', 'INVOCATION_EVENT_MASK', 'ITEM_EVENT_MASK', 'ITEM_FIRST', 'ITEM_LAST', 'ITEM_STATE_CHANGED', 'KEY_EVENT_MASK', 'MOUSE_EVENT_MASK', 'MOUSE_MOTION_EVENT_MASK', 'MOUSE_WHEEL_EVENT_MASK', 'PAINT_EVENT_MASK', 'RESERVED_ID_MAX', 'SELECTED', 'TEXT_EVENT_MASK', 'WINDOW_EVENT_MASK', 'WINDOW_FOCUS_EVENT_MASK', 'WINDOW_STATE_EVENT_MASK', '__class__', '__copy__', '__deepcopy__', '__delattr__', '__doc__', '__ensure_finalizer__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__str__', '__subclasshook__', '__unicode__', 'class', 'equals', 'getClass', 'getID', 'getItem', 'getItemSelectable', 'getSource', 'getStateChange', 'hashCode', 'item', 'itemSelectable', 'notify', 'notifyAll', 'paramString', 'setSource', 'source', 'stateChange', 'toString', 'wait']
        logger.debug("SequenceChoiceListener : event.item : %s" % str(event.item))
        logger.debug("SequenceChoiceListener : type(event.item) : %s" % str(type(event.item)))
        selected_sequence_id = event.item
        selected_sequence = self.catalog.sequences[selected_sequence_id]
        channel_ids = ['all'] + selected_sequence.get_channel_names()

        self.channel_choice.removeAll()
        for channel_id in channel_ids:
            self.channel_choice.add(channel_id)


def ask_for_sequence(catalog):
    title = 'select the sequence to process'
    gd = GenericDialog(title)
    # gd.addDialogListener(MyListener())
    sequence_ids = catalog.sequences.keys()
    sequence_ids.sort()
    assert len(sequence_ids) > 0
    default_sequence_id = sequence_ids[0]
    gd.addChoice('sequence', sequence_ids, default_sequence_id)
    channel_ids = ['all'] + catalog.sequences[default_sequence_id].get_channel_names()
    gd.addChoice('channel', channel_ids, 'all')
    choices = gd.getChoices()
    logger.debug("choices = %s" % choices)
    sequence_choice = choices[0]
    channel_choice = choices[1]
    sequence_choice.addItemListener(SequenceChoiceListener(channel_choice, catalog))
    gd.showDialog()
    if gd.wasCanceled():
        return {}
    selected_sequence_id = sequence_ids[gd.getNextChoiceIndex()]  # eg 'res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos2'
    selected_sequence = catalog.sequences[selected_sequence_id]
    selected_channel_index = gd.getNextChoiceIndex()
    if selected_channel_index == 0:
        selected_channel_id = 'all'
    else:
        selected_channel_id = selected_sequence.get_channel_names()[selected_channel_index - 1]  # eg 'DM300_327-353_fluo'
    logger.debug("chosen sequence : %s" % selected_sequence_id)
    logger.debug("chosen channel : %s" % selected_channel_id)
    return {'sequence': selected_sequence, 'channel_id':selected_channel_id}

def run_script():
    user_settings = UserSettings()
    IImageEngine.set_instance(IJImageEngine())
    catalog = ImageCatalog(user_settings.raw_images_root_path)
    if len(catalog.sequences) > 0:
        user_selection = ask_for_sequence(catalog)
        if len(user_selection) == 0:
            return
        sequence = user_selection['sequence']
        channel_id = str(user_selection['channel_id'])
        logger.debug("channel_id = %s (type = %s)" % (channel_id, str(type(channel_id))))
        if channel_id == 'all':
            sequence_as_stack = open_sequence_in_imagej(sequence)
        else: 
            hyperstack = sequence.as_hyperstack(selected_channel_ids=[channel_id], selected_frames=None, selected_slices=None)
            sequence_as_stack = hyperstack.hyperstack

        logger.debug('type(sequence_as_stack): ', type(sequence_as_stack))
        title = "%s:%s" % (sequence.id, channel_id)
        sequence_as_stack.setTitle(title)
        global SEQUENCE
        SEQUENCE = sequence_as_stack
    else:
        IJ.runMacro('waitForUser("Warning", "No sequences have been found in raw images root dir (%s)"' % user_settings.raw_images_root_path)


# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()

