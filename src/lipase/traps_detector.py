
from .catalog import ImageCatalog, Sequence
# from imageengine import Toto
from .imageengine import IImageEngine, Aabb, StackImageFeeder
from .imageengine import PixelType
from .telemos import WhiteEstimator, correct_non_uniform_lighting
from .maxima_finder import Match
from .template_matcher import TemplateMatcher

class TrapBinarizer(object):

    def __init__(self, flood_fill_start_point):
        self.flood_fill_start_point = flood_fill_start_point

    def binarize_trap(self, clean_trap_image):
        """
        :param IImage clean_trap_image:
        """
        ie = IImageEngine.get_instance()
        # convert image to edges because the grey level of traps is not uinform across the image
        # The threshold works much better on the edge image because the edges of the traps are clearly visible
        # Process/Find edges
        # https://imagej.nih.gov/ij/developer/api/ij/process/ImageProcessor.html#findEdges--
        edge_image = ie.compute_edge_transform(clean_trap_image)
        ie.debugger.on_image(edge_image, 'clean_trap_edge_image')
        # IJ.run(imp, "Find Edges", "");
        # after find edges, the walls are dark lines surrounded by 2 lines of clear pixels.
        # Image/Adjust/Threshold, choose auto, then click apply
        # IJ.setAutoThreshold(imp, "Default dark");
        (min_value, max_value) = edge_image.get_value_range()
        print('min_value = %f' % min_value)
        print('max_value = %f' % max_value)
        threshold_value = 0.20  # TODO: remove hardcoded value
        is_edge = ie.threshold( edge_image, max_value * threshold_value)
        # is_edge = ie.auto_threshold( edge_image)
        ie.debugger.on_image(is_edge, 'is_trap_edge')
        # convert image to mask
        # at this point, the walls are black lines surrounded by 2 thick lines of white pixels
        background_seed_pos = (70,34)
        outer_edge_merged = ie.flood_fill(is_edge, background_seed_pos)
        ie.debugger.on_image(outer_edge_merged, 'outer_edge_merged')
        # floodFill(70,34);
        # ij.process.FloodFiller
        # the start point of the flood fill is expected to be outside the trap so that the flood will merge with the outer white line, and stop at the wall position.
        # IJ.run(imp, "Invert");
        is_trap_outer_limit = ie.invert(outer_edge_merged)
        ie.debugger.on_image(is_trap_outer_limit, 'is_trap_outer_limit')
        # the wall is now white and surrounded wy black. Inside the walls we have the inner black line, plus som more white pixels in the trap's flat areas. Filling holes will then perge together what's not the outside; after this, all trap pixels are white.
        # IJ.run(imp, "Fill Holes", "");
        is_trap = ie.fill_holes(is_trap_outer_limit)
        ie.debugger.on_image(is_trap, 'is_trap')
        return is_trap

class TrapsDetector(object):

    def __init__(self, template_matcher):
        """
        :param TemplateMatcher template_matcher:
        """
        self.template_matcher = template_matcher

    def compute_traps_mask(self, sequence, channel_id, template_trap_aabb):
        """Remove the traps in the input sequence

        :param Sequence sequence:
        :param str channel_id:
        :param Aabb trap_aabb:
        :return Image: the mask of traps (binary image containing 0)

        the idea of this method is as follows:
        1. allow the user to provide an axis-aligned bounding box that contains a trap, and use this area as a pattern to be searched in all images of the sequence. Provided the tolerance is big enough, this should find all traps in the sequence.
        2. compute a clean (without globules) trap, by computing for each pixel, the median value from all traps for that pixel.
        3. For each trap location, remove the trap by subtracting the clean trap. This should leave the globules only.

        At the end of this image processing, we expect to only have the globules in the image.
        """
        uniform_stack = correct_non_uniform_lighting(sequence, channel_id, white_estimator=WhiteEstimator(open_size=75, close_size=75, average_size=75))

        first_image = uniform_stack.get_image(frame_index=0)
        template_trap_image = first_image.get_subimage(template_trap_aabb)

        ie = IImageEngine.get_instance()
        ie.debugger.on_image(template_trap_image, 'template_trap_image')

        matches = self.template_matcher.match_template(first_image, template_trap_image)
        num_traps_per_frame = len(matches)
        num_traps = uniform_stack.num_frames() * num_traps_per_frame
        
        traps_stack = ie.create_hyperstack(width=template_trap_aabb.width, height=template_trap_aabb.height, num_slices=1, num_frames=num_traps, num_channels=1, pixel_type=PixelType.F32)

        trap_index = 0
        for frame_index in range(uniform_stack.num_frames()):
            uniform_frame = uniform_stack.get_image(frame_index=frame_index)
            for frame_trap_index in range(num_traps_per_frame):
                match = matches[frame_trap_index]
                trap_aabb = Aabb(x_min=match.x, y_min=match.y, x_max=match.x + template_trap_aabb.width - 1, y_max=match.y + template_trap_aabb.height - 1)
                assert trap_aabb.width == template_trap_aabb.width
                trap_image = uniform_frame.get_subimage(trap_aabb)
                traps_stack.set_image(frame_index=trap_index, image=trap_image)
                trap_index += 1

        image_feeder = StackImageFeeder(traps_stack)

        clean_trap_image = ie.compute_median(image_feeder)
        ie.debugger.on_image(clean_trap_image, 'clean_trap_image')

        binarizer = TrapBinarizer((70,34))
        trap_mask = binarizer.binarize_trap(clean_trap_image)

        traps_mask = ie.create_image(width=uniform_stack.get_width(), height=uniform_stack.get_height(), pixel_type=PixelType.U8)
        for frame_trap_index in range(num_traps_per_frame):
            match = matches[frame_trap_index]
            traps_mask.set_subimage(trap_mask, (match.x, match.y))
        ie.debugger.on_image(traps_mask, 'traps_mask')

        #non_uniform_stack = sequence.as_stack()
        #uniform_stack = IImageEngine.get_instance().divide(non_uniform_stack, white_estimate)
        # IImageEngine.get_instance().debugger.on_image(white_estimate, 'white_estimate')
        return traps_mask
        