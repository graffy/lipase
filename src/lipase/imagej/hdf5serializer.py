# h5py is not available in fiji's jython
from ncsa.hdf.hdf5lib import HDFArray  # found in FIJI_HOME/jars/jhdf5-14.12.6.jar pylint: disable=import-error 

# samples in https://support.hdfgroup.org/HDF5/examples/hdf-java.html

from ncsa.hdf.hdf5lib.H5 import H5Fcreate, H5Fclose  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.H5 import H5Gcreate, H5Gclose  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.H5 import H5Screate_simple, H5Sclose  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.H5 import H5Dcreate, H5Dclose, H5Dwrite, H5Dget_type  # pylint: disable=import-error

from ncsa.hdf.hdf5lib.HDF5Constants import H5F_ACC_TRUNC  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.HDF5Constants import H5S_ALL  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.HDF5Constants import H5P_DEFAULT  # pylint: disable=import-error
from ncsa.hdf.hdf5lib.HDF5Constants import H5T_STD_U8LE, H5T_STD_U8LE, H5T_STD_U32LE, H5T_STD_I32LE, H5T_IEEE_F32LE, H5T_IEEE_F64LE, H5T_NATIVE_INT32  # pylint: disable=import-error

from jarray import zeros, array  # pylint: disable=import-error

from lipase.hdf5 import Group, ElementType, DataSet

def create_hdf5_test_file1(filepath):
    hdf5_file = H5Fcreate(filepath, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)

    group_id1 = H5Gcreate(hdf5_file, 'g1', H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)

    dataspace_id1 = H5Screate_simple(2, [20, 10], None)

    dataset_id = H5Dcreate(group_id1, "2D 32-bit integer 20x10", H5T_NATIVE_INT32, dataspace_id1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)

    H5Dclose(dataset_id)

    H5Sclose(dataspace_id1)

    H5Gclose(group_id1)

    H5Fclose(hdf5_file)

def create_hdf5_test_file2(filepath):
    hdf5_file = H5Fcreate(filepath, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)

    group_id1 = H5Gcreate(hdf5_file, 'g1', H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)

    dataspace_id1 = H5Screate_simple(1, [5], None)

    dataset_id = H5Dcreate(group_id1, "1D 32-bit integer 5", H5T_NATIVE_INT32, dataspace_id1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)
    refs=array([1,2,3,4,5], 'i')
    tid = H5Dget_type(dataset_id)
    # sid = H5.H5Dget_space(did);
    H5Dwrite(dataset_id, tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, refs)
    #    int[][] dataRead = new int[(int) dims2D[0]][(int) (dims2D[1])];

    #     try {
    #         if (dataset_id >= 0)
    #             H5.H5Dread(dataset_id, HDF5Constants.H5T_NATIVE_INT,
    #                     HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
    #                     HDF5Constants.H5P_DEFAULT, dataRead);

    H5Dclose(dataset_id)

    H5Sclose(dataspace_id1)

    H5Gclose(group_id1)

    H5Fclose(hdf5_file)

# https://www.jython.org/jython-old-sites/archive/21/docs/jarray.html
ELEMENT_TYPE_TO_JARRAY_TYPE={
    ElementType.U1: 'z',
    ElementType.U8: 'b',
    ElementType.U32: 'i',
    ElementType.S32: 'i',
    ElementType.F32: 'f',
    ElementType.F64: 'd',
    }

# https://support.hdfgroup.org/HDF5/doc/UG/HDF5_Users_Guide-Responsive%20HTML5/index.html#t=HDF5_Users_Guide%2FDatatypes%2FHDF5_Datatypes.htm
# https://support.hdfgroup.org/HDF5/doc/RM/PredefDTypes.html
ELEMENT_TYPE_TO_HDF5_TYPE={
    ElementType.U1: H5T_STD_U8LE,
    ElementType.U8: H5T_STD_U8LE,
    ElementType.U32: H5T_STD_U32LE,
    ElementType.S32: H5T_STD_I32LE,
    ElementType.F32: H5T_IEEE_F32LE,
    ElementType.F64: H5T_IEEE_F64LE,
    }


def _save_hdf5_data_set(hdf5_container, data_set):
    """
    :param hid_t hdf5_file: hdf5 file handle open in write mode or hdf5 group handle
    :param DataSet data_set:
    """

    dataspace_id = H5Screate_simple(data_set.dimension, list(data_set.size), None)

    element_type = data_set.element_type
    dataset_id = H5Dcreate(hdf5_container, data_set.name, ELEMENT_TYPE_TO_HDF5_TYPE[element_type], dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)

    data = array(data_set.elements, ELEMENT_TYPE_TO_JARRAY_TYPE[element_type])

    type_id = H5Dget_type(dataset_id)

    H5Dwrite(dataset_id, type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data)

    H5Dclose(dataset_id)

    H5Sclose(dataspace_id)


def _save_hdf5_group_contents(hdf5_container, group):
    for data_set in group.datasets.itervalues():
        _save_hdf5_data_set(hdf5_container, data_set)

    for data_set in group.groups.itervalues():
        _save_hdf5_group(hdf5_container, group)

def _save_hdf5_group(hdf5_container, group):
    """
    :param hid_t hdf5_container: hdf5 file handle open in write mode or hdf5 group handle
    :param Group group:
    """
    group_id1 = H5Gcreate(hdf5_container, group.name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)
    _save_hdf5_group_contents(hdf5_container, group)
    H5Gclose(group_id1)

def save_hdf5_file(filepath, group):
    """
    :param str filepath:
    :param Group group:
    """
    hdf5_file = H5Fcreate(filepath, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)

    # _save_hdf5_group(hdf5_file, group)
    _save_hdf5_group_contents(hdf5_file, group)

    H5Fclose(hdf5_file)


