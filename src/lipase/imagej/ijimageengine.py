"""Image processing using imagej.
"""

import os.path
from ..imageengine import IImage, IHyperStack, IImageEngine, PixelType, IImageProcessingDebugger, NullDebugger
from ..maxima_finder import Match
from .. import logger
from java.awt import Polygon
from ij import IJ, ImagePlus  # pylint: disable=import-error
from ij.process import FloodFiller # pylint: disable=import-error
from ij.process import Blitter # pylint: disable=import-error
from ij.measure import ResultsTable # pylint: disable=import-error
from ij.plugin import ImageCalculator  # pylint: disable=import-error
from ij.plugin.filter import MaximumFinder # pylint: disable=import-error
from ij.plugin import ZProjector # pylint: disable=import-error
from ij.gui import ImageRoi # pylint: disable=import-error
from ijopencv.ij import ImagePlusMatConverter  # pylint: disable=import-error
from ijopencv.opencv import MatImagePlusConverter  # pylint: disable=import-error
import org.bytedeco.javacpp.opencv_core as opencv_core  # pylint: disable=import-error
import org.bytedeco.javacpp.opencv_imgproc as opencv_imgproc  # pylint: disable=import-error
# from org.bytedeco.javacpp.opencv_imgcodecs import imread, imwrite  # pylint: disable=import-error


IJ_PIXELTYPE_TO_PIXEL_TYPE = {
    ImagePlus.GRAY8: PixelType.U8,
    ImagePlus.GRAY16: PixelType.U16,
    ImagePlus.GRAY32: PixelType.F32
    }

PIXEL_TYPE_TO_CV_PIXEL_TYPE = {
    PixelType.U8: opencv_core.CV_8U,
    PixelType.U16: opencv_core.CV_16U,
    PixelType.F32: opencv_core.CV_32F,
}

def clone_processor(src_processor, clone_pixel_type):
    """
    :param Processor processor:
    :param PixelType clone_pixel_type:
    :rtype: Processor
    """
    clone_processor = { 
        PixelType.U8: src_processor.convertToByteProcessor(),
        PixelType.U16: src_processor.convertToShortProcessor(),
        PixelType.F32: src_processor.convertToFloatProcessor(),
         }[clone_pixel_type]
    return clone_processor

class IJImage(IImage):

    def __init__(self, image_engine, ij_image=None, width=None, height=None, pixel_type=None):
        """
        :param IJImageEngine image_engine:
        :param ImagePlus ij_image:
        """
        self.image_engine = image_engine
        if ij_image is not None:
            assert ij_image.getNSlices() == 1
            assert ij_image.getNFrames() == 1
            assert ij_image.getNChannels() == 1
            self.ij_image = ij_image
        else:
            assert isinstance(width, int)
            assert isinstance(height, int)
            assert isinstance(pixel_type, int)
            stack_name = ''
            self.ij_image = IJ.createHyperStack(stack_name, width, height, 1, 1, 1, PixelType.get_num_bits(pixel_type))


    def clone(self, clone_pixel_type=None):
        if clone_pixel_type == None:
            clone_pixel_type = self.get_pixel_type()
        copy = IJImage(self.image_engine, width=self.get_width(), height=self.get_height(), pixel_type=clone_pixel_type)
        src_processor = self.ij_image.getProcessor()
        copy.ij_image.setProcessor( clone_processor(src_processor, clone_pixel_type) )
        assert copy.get_pixel_type() == clone_pixel_type
        return copy

    def get_width(self):
        return self.ij_image.width

    def get_height(self):
        return self.ij_image.height

    def get_pixel_type(self):
        ij_pixel_type = self.ij_image.getType()
        return IJ_PIXELTYPE_TO_PIXEL_TYPE[ij_pixel_type]

    def set_pixel(self, x, y, value):
        self.ij_image.getProcessor().putPixelValue(x, y, value)

    def get_subimage(self, aabb):
        self.ij_image.saveRoi()
        self.ij_image.setRoi(aabb.x_min, aabb.y_min, aabb.width, aabb.height)
        ij_subimage = self.ij_image.crop()
        self.ij_image.restoreRoi()

        # ij_subimage = self.image_engine.create_hyperstack(width=aabb.width, height=aabb.height, num_channels=1, num_slices=1, num_frames=1, pixel_type=self.get_pixel_type())
        # ij_subimage.paste()
        return IJImage(self.image_engine, ij_subimage)

    def set_subimage(self, subimage, position):
        (x, y) = position
        src_processor = subimage.ij_image.getProcessor()
        dst_processor = self.ij_image.getProcessor()
        dst_processor.copyBits(src_processor, x, y, Blitter.COPY)

    def get_value_range(self):
        processor = self.ij_image.getProcessor()
        # processor.getMin() and processor.getMax() give a higher min and a lower max, probably because they only consider a subpart of the image (this is badly documented)
        # so we use processor.getStats instead
        image_stats = processor.getStats()  # :type ImageStatistics image_stats:
        min_value = image_stats.min
        max_value = image_stats.max
        return (min_value, max_value)

    def get_mean_value(self):
        processor = self.ij_image.getProcessor()
        image_stats = processor.getStats()  # :type ImageStatistics image_stats:
        return image_stats.mean

    def scale_values(self, scale):
        processor = self.ij_image.getProcessor()
        processor.multiply(scale)

    def set_masked_pixels(self, mask, pixel_value):
        assert isinstance(mask, IJImage)
        assert mask.get_pixel_type() != PixelType.F32, "imagej doesn't work with floating point masks (the mask is ignored)"
        assert mask.get_pixel_type() == PixelType.U8

        # https://imagej.nih.gov/ij/developer/api/index.html setRoi
        x = 0
        y = 0
        image_plus = self.ij_image

        # image_plus.setMask(mask)
        # image_roi = ImageRoi(x, y, mask.ij_image.getProcessor())
        # image_plus.setRoi(image_roi)
        image_plus.getProcessor().snapshot()
        #image_plus.getProcessor().setRoi(image_roi)
        #image_plus.getProcessor().setRoi(p)

        # image_plus.setValue(pixel_value)  # no such method ???
        # image_plus.setColor(Color())
        # image_plus.getProcessor().setColor(pixel_value)
        # image_plus.fill() # no such method ???
        # image_plus.getProcessor().set(pixel_value) # doesn't seem to respect roi as said in doc
        image_plus.getProcessor().setValue(pixel_value)
        image_plus.getProcessor().fill()
        # image_plus.getProcessor().reset(image_plus.getProcessor().getMask())
        image_plus.getProcessor().reset(mask.ij_image.getProcessor())
        # image_plus.getProcessor().fill(mask.ij_image.getProcessor())  
        #   File "__pyclasspath__/lipase/imagej/ijimageengine.py", line 141, in set_masked_pixels
        #     image_plus.getProcessor().fill(mask.ij_image.getProcessor())
        # ClassCastException: java.lang.ClassCastException: [F cannot be cast to [B

        # image_plus.killRoi()
        # image_plus.setRoi(None)
        # image_plus.setMask(mask)
        
        # IJ.run(imp, "Create Selection", "");
        # mask = imp.createRoiMask();
        # IJ.run("Restore Selection", "");
        # IJ.run(imp, "Set...", "value=12345");



class IJHyperStack(IHyperStack):

    def __init__(self, image_engine, width, height, num_channels, num_slices, num_frames, pixel_type):
        """
        """
        self.image_engine = image_engine
        stack_name = ''
        self.hyperstack = IJ.createHyperStack(stack_name, width, height, num_channels, num_slices, num_frames, PixelType.get_num_bits(pixel_type))

    def set_image(self, image, frame_index=0, slice_index=0, channel_index=0):
        self.hyperstack.setPositionWithoutUpdate(channel_index + 1, slice_index + 1, frame_index + 1)
        self.hyperstack.setProcessor(image.ij_image.getProcessor())

    def get_image(self, frame_index=0, slice_index=0, channel_index=0):
        self.hyperstack.setPositionWithoutUpdate(channel_index + 1, slice_index + 1, frame_index + 1)
        stack_name = '' 
        image_plus = IJ.createHyperStack(stack_name, self.get_width(), self.get_height(), 1, 1, 1, PixelType.get_num_bits(self.get_pixel_type()))
        image_plus.setProcessor(clone_processor(self.hyperstack.getProcessor(), self.get_pixel_type()))
        ij_image = IJImage(self.image_engine, image_plus)
        return ij_image

    def get_width(self):
        return self.hyperstack.width

    def get_height(self):
        return self.hyperstack.height

    def num_slices(self):
        return self.hyperstack.getNSlices()

    def num_frames(self):
        return self.hyperstack.getNFrames()

    def num_channels(self):
        return self.hyperstack.getNChannels()

    def get_pixel_type(self):
        ij_pixel_type = self.hyperstack.getType()
        
        return IJ_PIXELTYPE_TO_PIXEL_TYPE[ij_pixel_type]



def imagej_run_image_command(image, command, options):
    """performs the given imagej command on the given image

    :param ImagePlus image:
    :param str command: imagej command (eg "Gray Morphology")
    :param str options: imagej options (eg "radius=1 type=square operator=open")

    wrapper around IJ.run (https://imagej.nih.gov/ij/developer/api/ij/IJ.html#run-ij.ImagePlus-java.lang.String-java.lang.String-) which raises an exception on error
    """
    IJ.run(image, command, options)
    error_message = IJ.getErrorMessage()
    if error_message is not None:
        raise Exception('The command "%s" with options "%s" failed because of the following error : %s' % (command, options, error_message))


def perform_gray_morphology_with_imagej(image, operator, structuring_element_shape, structuring_element_radius):
    """
    :param IJImage image:
    :param str operator: eg 'open'
    :param str structuring_element_shape: eg 'square'
    :param int structuring_element_radius:
    """
    assert operator not in ['fast open', 'fast erode'], "as of 13/09/2019, fast operators such as 'fast erode' seem broken in fiji (the resulting image is not at all similar to their slow equivalent)"
    
    processor = image.ij_image.getProcessor()
    if processor.getBitDepth() != 8:
        min_value = processor.getMin()
        max_value = processor.getMax()
        print("warning: downgrading image to byte as imagej's Gray Morphology processing doesn't handle 16bit images (range=[%d, %d])" % (min_value, max_value))
        do_scaling = True
        image.ij_image.setProcessor(processor.convertToByte(do_scaling))
    print("before gray morphology")
    assert structuring_element_radius < 11, "the radius of the structuring element is too big (%d); using it with Fiji's 'Gray Morphology' tool would result in very long computations." % structuring_element_radius
    imagej_run_image_command(image.ij_image, "Gray Morphology", "radius=%d type=%s operator=%s" % (structuring_element_radius, structuring_element_shape, operator))
    print("after gray morphology")


def perform_gray_morphology_with_ijopencv(image, operator, structuring_element_shape, structuring_element_radius):
    """Perform gray morphology on the given image using imagej's opencv api (this api is provided by javacpp-presets)

    :param IJImage image:
    :param str operator: eg 'open'
    :param str structuring_element_shape: eg 'square'
    :param int structuring_element_radius:
    """

    shape_name_to_opencv_id = {
        'square': opencv_imgproc.MORPH_RECT,
    }

    operator_name_to_opencv_id = {
        'open': opencv_imgproc.MORPH_OPEN,
        'close': opencv_imgproc.MORPH_CLOSE,
    }

    if structuring_element_shape not in shape_name_to_opencv_id.keys():
        raise NotImplementedError('structuring element shape %s is not currently supported (supported operators : %s)' % (structuring_element_shape, str(operator_name_to_opencv_id.keys())))

    if operator not in operator_name_to_opencv_id.keys():
        raise NotImplementedError('operator %s is not currently supported (supported operators : %s)' % (operator, str(operator_name_to_opencv_id.keys())))

    imp2mat = ImagePlusMatConverter()
    cv_src_image = imp2mat.toMat(image.ij_image.getProcessor())

    # cv_src_image = opencv_core.Mat().zeros(128, 128, opencv_core.CV_8U).asMat()
    print(cv_src_image)

    struct_el_size = structuring_element_radius * 2 + 1

    struct_el_size = opencv_core.Size(struct_el_size, struct_el_size)
    struct_el_anchor = opencv_core.Point(structuring_element_radius, structuring_element_radius)
    # print(struct_el_anchor)
    struct_element = opencv_imgproc.getStructuringElement(shape_name_to_opencv_id[structuring_element_shape], struct_el_size, struct_el_anchor)
    print(struct_element)
    # dst_image = opencv_core.Mat().zeros(128, 128, opencv_core.CV_8U).asMat()
    cv_dst_image = opencv_core.Mat(cv_src_image.size(), cv_src_image.type())

    # warning ! trying to guess the arguments from opencv's documentation is time consuming as the error messages are misleading (in the following call, javacpp complains that the 1st argument is not a org.bytedeco.javacpp.opencv_core$Mat, while it is ! The problem comes from the order of the arguments). So, instead of guessing, the found accepted signatures of morphologyEx are in javacpp-presets/opencv/src/gen/java/org/bytedeco/opencv/global/opencv_imgproc.java
    # opencv_imgproc.morphologyEx(cv_src_image, opencv_imgproc.MORPH_OPEN, struct_element, dst_image)
    # TypeError: morphologyEx(): 1st arg can't be coerced to org.bytedeco.javacpp.opencv_core$GpuMat, org.bytedeco.javacpp.opencv_core$Mat, org.bytedeco.javacpp.opencv_core$UMat
    print('before opencv_imgproc.morphologyEx')
    opencv_imgproc.morphologyEx(cv_src_image, cv_dst_image, operator_name_to_opencv_id[operator], struct_element)
    print('after opencv_imgproc.morphologyEx')

    mat2imp = MatImagePlusConverter()
    image.ij_image.setProcessor(mat2imp.toImageProcessor(cv_dst_image))


class ImageLogger(IImageProcessingDebugger):

    def __init__(self):
        IImageProcessingDebugger.__init__(self)

    def on_image(self, image, image_id):
        # todo : WindowManager
        image.show()

class IJImageEngine(IImageEngine):

    def __init__(self, debugger=NullDebugger()):
        IImageEngine.__init__(self, debugger)

    def missing(self):
        pass

    def create_hyperstack(self, width, height, num_channels, num_slices, num_frames, pixel_type):
        return IJHyperStack(self, width, height, num_channels, num_slices, num_frames, pixel_type)

    def create_image(self, width, height, pixel_type):
        return IJImage(self, width=width, height=height, pixel_type=pixel_type)

    def load_image(self, src_image_file_path):
        image_plus = IJ.openImage(src_image_file_path)
        return IJImage(self, image_plus)

    def save_image_as_tiff(self, image, out_file_path):
        IJ.saveAsTiff(image.ij_image, out_file_path)
        # IJ.saveAsTiff catches io exception in case the file couldn't be saved. So we have to check if the operation succeeded in another way
        if not os.path.isfile(out_file_path):
            assert False, 'failed to create %s' % out_file_path

    def save_hyperstack_as_tiff(self, hyperstack, out_file_path):
        IJ.saveAsTiff(hyperstack.hyperstack, out_file_path)
        # IJ.saveAsTiff catches io exception in case the file couldn't be saved. So we have to check if the operation succeeded in another way
        if not os.path.isfile(out_file_path):
            assert False, 'failed to create %s' % out_file_path

    def subtract(self, image1, image2):
        ic = ImageCalculator()
        result_image = ic.run("Subtract create float", image1.ij_image, image2.ij_image)
        return IJImage(self, result_image)

    def multiply(self, image1, image2):
        ic = ImageCalculator()
        result_image = ic.run("Multiply create float", image1.ij_image, image2.ij_image)
        return IJImage(self, result_image)

    def divide(self, numerator_image, denominator_image):
        ic = ImageCalculator()
        result_image = ic.run("Divide create float", numerator_image.ij_image, denominator_image.ij_image)
        return IJImage(self, result_image)

    def abs(self, image):
        result_image = image.clone()
        result_image.ij_image.getProcessor().abs()
        return result_image

    def compute_edge_transform(self, image):
        result_image = image.clone()
        result_image.ij_image.getProcessor().findEdges()
        return result_image

    def compute_max(self, image_feeder):
        #def compute_max(self, images_file_path):
        """Computes for each pixel position the maximum at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IJImage
        """
        it = iter(image_feeder)

        # assert len(images_file_path) > 1
        max_image = it.next().ij_image

        #max_image = IJ.openImage(images_file_path[0])
        #print('max_image', max_image)

        # for image_file_path in images_file_path[2:-1]:
        for other_image in it:
            # other_image = IJ.openImage(image_file_path)
            ic = ImageCalculator()
            ic.run("max", max_image, other_image.ij_image)
        return IJImage(self, max_image)

    def _zproject(self, image_feeder, operator):
        """
        Performs a reduction along the stack's z axis, using one of the reduction operators

        Parameters
        ----------
        image_feeder: IImageFeeder
            the provider of input images along z axis
        operator: str
            one of the reduction operators supported by imagej's ZProjector (https://imagej.nih.gov/ij/developer/api/ij/plugin/ZProjector.html)

        Returns
        -------
        projected_image: IImage
            the image resulting of the reduction
        """
        assert operator in ['median', 'average', 'sd']
        hyperstack = image_feeder.create_hyperstack()
        # https://imagej.nih.gov/ij/developer/api/ij/plugin/ZProjector.html
        projector = ZProjector()
        projection_image = projector.run(hyperstack.hyperstack, operator)
        # imagej_run_image_command(image=hyperstack.hyperstack, command="Z Project...", options="projection=Median")
        # # after median computation, the resulting image is expected to be the selected one
        # median_image = IJ.getImage()  # get the currently selected image
        return IJImage(self, projection_image)


    def compute_median(self, image_feeder):
        """Computes for each pixel position the median value at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IJmage
        """
        return self._zproject(image_feeder, 'median')

    def compute_mean(self, image_feeder):
        return self._zproject(image_feeder, 'average')

    def compute_variance(self, image_feeder):
        stddev_image = self.compute_stddev(image_feeder)
        variance_image = self.multiply(stddev_image, stddev_image)
        return variance_image

    def compute_stddev(self, image_feeder):
        return self._zproject(image_feeder, 'sd')


    def mean_filter(self, image, radius):
        """Implement interface method."""
        IJ.run(image.ij_image, "Mean...", "radius=%d" % radius)

    def filter2D(self, src_image, dst_type, kernel, anchor=(-1, -1)):
        if anchor == (-1, -1):
            assert kernel.get_width() % 2 == 1 and kernel.get_height() % 2 == 1, "kernel sizes are expected to be odd if you want the anchor to be at the center of the kernel"

            anchor = ((kernel.get_width() - 1) / 2, (kernel.get_height() - 1) / 2)

        imp2mat = ImagePlusMatConverter()
        cv_src_image = imp2mat.toMat(src_image.ij_image.getProcessor())
        cv_kernel = imp2mat.toMat(kernel.ij_image.getProcessor())

        cv_anchor = opencv_core.Point(anchor[0], anchor[1])
        ie = IImageEngine.get_instance()
        dst_image = ie.create_image(width=src_image.get_width(), height=src_image.get_height(), pixel_type=PixelType.F32)
        cv_dst_image = opencv_core.Mat(cv_src_image.size(), cv_src_image.type())

        # warning ! trying to guess the arguments from opencv's documentation is time consuming as the error messages are misleading (in the following call, javacpp complains that the 1st argument is not a org.bytedeco.javacpp.opencv_core$Mat, while it is ! The problem comes from the order of the arguments). So, instead of guessing, the found accepted signatures of filter2D are in https://github.com/bytedeco/javacpp-presets/blob/master/opencv/src/gen/java/org/bytedeco/opencv/global/opencv_imgproc.java
        # opencv_imgproc.morphologyEx(cv_src_image, opencv_imgproc.MORPH_OPEN, struct_element, dst_image)
        # TypeError: morphologyEx(): 1st arg can't be coerced to org.bytedeco.javacpp.opencv_core$GpuMat, org.bytedeco.javacpp.opencv_core$Mat, org.bytedeco.javacpp.opencv_core$UMat
        delta = 0
        border_type = opencv_core.BORDER_DEFAULT
        if dst_type is None:
            ddepth = -1
        else:
            ddepth = PIXEL_TYPE_TO_CV_PIXEL_TYPE[dst_type]
        opencv_imgproc.filter2D(cv_src_image, cv_dst_image, ddepth, cv_kernel, cv_anchor, delta, border_type)

        mat2imp = MatImagePlusConverter()
        dst_image.ij_image.setProcessor(mat2imp.toImageProcessor(cv_dst_image))
        return dst_image

    def perform_gray_morphology(self, image, operator, structuring_element_shape, structuring_element_radius):
        """
        :param IJImage image:
        :param str operator: eg 'open'
        :param str structuring_element_shape: eg 'square'
        :param int structuring_element_radius:
        """
        # we use opencv version because imagej's slow version is way too slow for big values of structuring_element_radius
        perform_gray_morphology_with_ijopencv(image, operator, structuring_element_shape, structuring_element_radius)

    def replace_border(self, image, band_width):
        """Implement interface method."""
        raise NotImplementedError()
        return image  # pylint: disable=unreachable

    def match_template(self, src_image, template_image):
        cv_match_template_supported_pixel_types = [PixelType.U8, PixelType.F32]
        if src_image.get_pixel_type() not in cv_match_template_supported_pixel_types:
            logger.debug('converting src_image')
            src_image = src_image.clone(PixelType.F32)
        if template_image.get_pixel_type() not in cv_match_template_supported_pixel_types:
            logger.debug('converting template_image')
            template_image = template_image.clone(PixelType.F32)
        # import org.opencv.imgproc.Imgproc;
        #
        # /*
		#  * CV_TM_SQDIFF = 0, CV_TM_SQDIFF_NORMED = 1, CV_TM_CCORR = 2,
		#  * CV_TM_CCORR_NORMED = 3, CV_TM_CCOEFF = 4, CV_TM_CCOEFF_NORMED = 5;
		#  */
        correlation_method_id = opencv_imgproc.CV_TM_SQDIFF
		# Imgproc.matchTemplate(src, tpl, correlationImage, toCvCorrelationId(correlationMethodId));
        imp2mat = ImagePlusMatConverter()
        cv_src_image = imp2mat.toMat(src_image.ij_image.getProcessor())
        cv_template_image = imp2mat.toMat(template_image.ij_image.getProcessor())
        similarity_image_size = opencv_core.Size(src_image.get_width()-template_image.get_width()+1, src_image.get_width()-template_image.get_height()+1)
        cv_similarity_image = opencv_core.Mat(similarity_image_size, cv_src_image.type())

        # warning ! trying to guess the arguments from opencv's documentation is time consuming as the error messages are misleading (in the following call, javacpp complains that the 1st argument is not a org.bytedeco.javacpp.opencv_core$Mat, while it is ! The problem comes from the order of the arguments). So, instead of guessing, the found accepted signatures of cvMatchTemplate are in https://github.com/bytedeco/javacpp-presets/blob/master/opencv/src/gen/java/org/bytedeco/opencv/global/opencv_imgproc.java


        # print(cv_src_image)
        # print(cv_template_image)
        # print(cv_similarity_image)

        if False:
            cv_src_image = opencv_core.Mat(cv_src_image.size(), opencv_core.CV_8U)
            cv_template_image = opencv_core.Mat(cv_template_image.size(), opencv_core.CV_8U)
            cv_similarity_image = opencv_core.Mat(cv_similarity_image.size(), opencv_core.CV_8U)
            print(cv_src_image)
            print(cv_template_image)
            print(cv_similarity_image)

        opencv_imgproc.matchTemplate( cv_src_image, cv_template_image, cv_similarity_image, correlation_method_id)

        if correlation_method_id in [ opencv_imgproc.CV_TM_SQDIFF, opencv_imgproc.CV_TM_SQDIFF_NORMED ]:
            scale = -1.0
            # opencv_core.multiply(cv_similarity_image, opencv_core.Scalar.all(scale), cv_similarity_image)
            # cv_similarity_image = opencv_core.Mat(similarity_image_size, cv_src_image.type())
            # cv_similarity_image = opencv_core.Mat(cv_similarity_image.size(), opencv_core.CV_8U)
            cv_similarity_image = opencv_core.multiply(cv_similarity_image, scale).asMat()
            # print(cv_similarity_image)

        similarity_image = self.create_image(
            width=src_image.get_width()-template_image.get_width()+1,
            height=src_image.get_height()-template_image.get_height()+1,
            pixel_type=PixelType.F32)

        mat2imp = MatImagePlusConverter()
        similarity_image.ij_image.setProcessor(mat2imp.toImageProcessor(cv_similarity_image))

        return similarity_image

    def find_maxima(self, src_image, threshold, tolerance):
        # to understand the meaning of threshold ansd maxima, I had to look at the source code in https://github.com/imagej/imagej1/blob/master/ij/plugin/filter/MaximumFinder.java#L636
        # 1. The algorithm first builds the list of maxima candidates. Only local maxima above this threshold are considered as valid maxima candidates
        # 2. Then each maximum candidate is processed to find its neighborhood area, starting from the highest. For ecach candidate, a propagation is performed on neighbours as long as the neighbour value is within the tolerance of the maximum candidate. If another candidate is encountered as a neighbour, then this candidate is removed as it('s been absorbed by the current maximum candidate)

        matches = []

        output_type = MaximumFinder.LIST
        exclude_on_edges = False
        is_euclidean_distance_map = False
        max_finder = MaximumFinder()
        max_finder.findMaxima(src_image.ij_image.getProcessor(), tolerance, threshold, output_type, exclude_on_edges, is_euclidean_distance_map)
        results_table = ResultsTable.getResultsTable()
        num_points = results_table.getCounter()
        # print("number of matches : %d" % num_points)
        for match_index in range(num_points):
            x = int(results_table.getValue('X', match_index))
            y = int(results_table.getValue('Y', match_index))
            correlation = src_image.ij_image.getProcessor().getf(x, y)
            match = Match(x, y, correlation)
            # print(match)
            matches.append(match)
        return matches

    def threshold(self, src_image, threshold_value):
        binary_image = src_image.clone()
        IJ.setThreshold(binary_image.ij_image, threshold_value, 1.e30)  # TODO:remove hardcoded value (1.0e30 is the value imagej chooses when the user moves the threshold slider to the right)
        IJ.run(binary_image.ij_image, "Convert to Mask", "")
        return binary_image

    def auto_threshold(self, src_image):
        # Image/Adjust/Threshold, choose auto, then click apply
        # IJ.setAutoThreshold(imp, "Default dark");
        binary_image = src_image.clone()
        IJ.setAutoThreshold(binary_image.ij_image, "Default dark")
        IJ.run(binary_image.ij_image, "Convert to Mask", "")
        return binary_image

    def flood_fill(self, src_binary_image, flood_start_pos):
        # https://imagej.nih.gov/ij/developer/api/ij/process/FloodFiller.html
        flood_filled_image = src_binary_image.clone()
        image_processor = flood_filled_image.ij_image.getProcessor()
        image_processor.setValue(255)
        flood_filler = FloodFiller(image_processor)
        (x, y) = flood_start_pos
        flood_filler.fill(x, y)
        return flood_filled_image

    def fill_holes(self, src_binary_image):
        result_image = src_binary_image.clone()
        IJ.run(result_image.ij_image, "Fill Holes", "")
        return result_image        

    def invert(self, src_binary_image):
        inverted_image = src_binary_image.clone()
        image_processor = inverted_image.ij_image.getProcessor()
        image_processor.invert()
        return inverted_image
        
