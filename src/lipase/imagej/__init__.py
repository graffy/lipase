import os
from ij import IJ, ImagePlus  # pylint: disable=import-error
import re
from .. import logger
from ..catalog import MissingImageFile

def open_sequence_as_hyperstack(sequence):
    hyperstack = IJ.createHyperStack(sequence.id, sequence.width, sequence.height, sequence.num_channels, sequence.num_slices, sequence.num_frames, sequence.num_bits_per_pixels)
    for channel_index in range(sequence.num_channels):
        for frame_index in range(sequence.num_frames):
            slice_index = 0
            try:
                src_image_file_path = sequence.get_image_file_path(channel_index=channel_index, frame_index=frame_index)
                src_image = IJ.openImage(src_image_file_path)
            except MissingImageFile as e:
                logger.warn('%s. As a result, the hyperstack will be black at this frame' % str(e))
                #logger.warn('the image file for frame %d and channel %d is missing. As a result, the hyperstack will be black at this frame' % (frame_index, channel_index))
            # print(src_image.getProperties())
            hyperstack.setPositionWithoutUpdate(channel_index + 1, slice_index + 1, frame_index + 1)
            hyperstack.setProcessor(src_image.getProcessor())
    return hyperstack

def open_sequence_as_stack(sequence, channel_id):
    '''
    :param str channel_id: eg 'DM300_327-353_fluo'
    '''
    channel_index = sequence.get_channel_index(channel_id)
    hyperstack = IJ.createHyperStack(sequence.id, sequence.width, sequence.height, 1, sequence.num_slices, sequence.num_frames, sequence.num_bits_per_pixels)
    for frame_index in range(sequence.num_frames):
        slice_index = 0
        try:
            src_image_file_path = sequence.get_image_file_path(channel_index=channel_index, frame_index=frame_index)
            # print(src_image_file_path)
            src_image = IJ.openImage(src_image_file_path)
        except MissingImageFile as e:
            logger.warn('%s. As a result, the hyperstack will be black at this frame' % str(e))
            # logger.warn('the image file for frame %d and channel %d is missing. As a result, the hyperstack will be black at this frame' % (frame_index, channel_index))
        # print(src_image.getProperties())
        hyperstack.setPositionWithoutUpdate(channel_index + 1, slice_index + 1, frame_index + 1)
        hyperstack.setProcessor(src_image.getProcessor())
    return hyperstack

def open_sequence_in_imagej(sequence):
    # ip = IJ.createHyperStack(title=sequence.id, width=sequence.width, height= sequence.height, channels=1, slices=1, frames=sequence.get_num_frames(), bitdepth=16)
    hyperstack = open_sequence_as_hyperstack(sequence)
    hyperstack.show()
    for channel_index in range(sequence.num_channels):
        hyperstack.setPositionWithoutUpdate(channel_index + 1, 1, 1)
        IJ.run("Enhance Contrast", "saturated=0.35")
    return hyperstack

def ij_version_as_float(ij_version):
    '''
    ij_version : something like '1.53f51'
    '''
    match = re.match('(?P<n1>[0-9]+).(?P<n2>[0-9]+)(?P<n3>[a-z])(?P<n4>[0-9]+)', ij_version)
    assert match
    n3_as_number = ord(match.group('n3')) - ord('a')
    assert n3_as_number >= 0
    assert n3_as_number < 26
    no_letter_version = match.group('n1')+'.'+ match.group('n2') + ('%02d' % (n3_as_number)) + match.group('n4')
    return float(no_letter_version)


def imagej_has_headless_bug():
    # https://forum.image.sc/t/processing-filter-headless-in-jython/48055/2
    ij_full_version = IJ.getFullVersion()  # something like '1.53f51'
    this_ij_version = ij_version_as_float(ij_full_version)
    return this_ij_version >= ij_version_as_float('1.52q00') and this_ij_version < ij_version_as_float('1.53h55')


