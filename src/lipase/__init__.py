# import sys
import logging
import logging.handlers
import logging
import os
from ij import IJ


class IJLogger():
    '''a logger with the same interface as logging.getLogger() but which uses imagej's log() mechanism

    todo: apparently, IJ.log() logs an INFO message by default. I don't know how to log messages with other severities (WARNING, ERRORS). So, instead, I prepend the severity as a string to the message, which is not great...
    '''
    def debug(self, message):
        IJ.log('DEBUG : %s' % message)

    def info(self, message):
        IJ.log('INFO : %s' % message)

    def warn(self, message):
        IJ.log('WARN : %s' % message)

    def error(self, message):
        IJ.log('ERROR : %s' % message)



def create_logger():
    logger_type = 'std_python_logger'  # 'std_python_logger' or 'ij_logger'
    if logger_type == 'std_python_logger':
        # logging.basicConfig( stream=sys.stderr )
        handler = logging.FileHandler(os.environ.get("LOGFILE", "/tmp/lipase.log"))
        formatter = logging.Formatter(logging.BASIC_FORMAT)
        handler.setFormatter(formatter)
        logger = logging.getLogger(__name__)
        logger.setLevel( logging.DEBUG )
        logger.addHandler(handler)
    elif logger_type == 'std_python_logger':
        logger = IJLogger()
    else:
        assert False
    return logger

logger = create_logger()

