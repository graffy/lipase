
import os
import json
from .imageengine import IHyperStack, IImageEngine, PixelType
from . import logger

class DacMetadata(object):
    """Represents a display_and_comments.txt metadata file.
    
    display_and_comments.txt is a metadata file stored by micro manager
    """
    
    def __init__(self, dac_id, dac_file_path):
        """Contructor.

        :param str dac_id: eg ``res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1``
        :param str dac_file_path: eg ``/Users/graffy/ownCloud/ipr/lipase/raw-images/res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/display_and_comments.txt``
        """
        self.dac_id = dac_id
        self.dac_file_path = dac_file_path
        with open(dac_file_path, "r") as dac_file:
            self.dac = json.load(dac_file, encoding='latin-1')  # note : the micromanager metadata files are encoded in latin-1, not utf8 (see accents in comments)

    def get_channel_name(self, channel_index):
        """Returns the name of the given channel index
        """
        channels = self.dac['Channels']
        return channels[channel_index]["Name"]


class MissingImageFile(Exception):
    
    def __init__(self, sequence_id, frame_index, slice_index, channel_index):
        self.sequence_id = sequence_id
        self.frame_index= frame_index
        self.slice_index = slice_index
        self.channel_index = channel_index
        super(MissingImageFile, self).__init__(self, 'missing image file')

    def __str__(self):
        return 'the image file for sequence "%s" frame %d, slice %d and channel %d is missing' % (self.sequence_id, self.frame_index, self.slice_index, self.channel_index)


class Sequence(object):
    """A sequence of images stored in micro manager format
    """
    def __init__(self, catalog, sequence_id, micro_manager_metadata_file_path):
        """
        :param ImageCatalog catalog:
        :param str sequence_id: eg ``res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0``
        :param str micro_manager_metadata_file_path: eg ``/Users/graffy/ownCloud/ipr/lipase/raw-images/res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0/metadata.txt``
        """
        self.catalog = catalog
        self.id = sequence_id
        self.micro_manager_metadata_file_path = micro_manager_metadata_file_path
        logger.debug('reading micro manager metatdata from %s' % micro_manager_metadata_file_path)

        # note : the micromanager metadata files are encoded in latin-1, not utf8 (see accents in comments)
        with open(os.path.realpath(micro_manager_metadata_file_path), "r") as mmm_file:
            self.mmm = json.load(mmm_file, encoding='latin-1')  # note : the micromanager metadata files are encoded in latin-1, not utf8 (see accents in comments)

        (pos_dir_path, file_name) = os.path.split(micro_manager_metadata_file_path)  # pylint: disable=unused-variable
        (micro_manager_metadata_file_parent_path, pos_dir_name) = os.path.split(pos_dir_path)  # pylint: disable=unused-variable
        assert pos_dir_name[0:3] == 'Pos', 'unexpected value : %s is expected to be of the form "Pos<n>"' % pos_dir_name
        logger.debug('micro_manager_metadata_file_parent_path = %s' % micro_manager_metadata_file_parent_path)

        dac_file_path = os.path.join(micro_manager_metadata_file_parent_path, 'display_and_comments.txt')
        (dac_id, file_name) = os.path.split(self.id)
        self.dac = DacMetadata(dac_id, dac_file_path)

        for channel_index in range(self.num_channels):
            assert self.get_channel_name(channel_index) == self.dac.get_channel_name(channel_index), 'mismatched channel names for channel index %d : %s in %s, %s in %s' % (channel_index, self.get_channel_name(channel_index), self.micro_manager_metadata_file_path, self.dac.get_channel_name(channel_index), self.dac.dac_file_path)

    @property
    def num_frames(self):
        """Gets the number of frames in the sequence
        """
        summary = self.mmm['Summary']
        num_frames = int(summary['Frames'])
        # handle the case where the recording has been cancelled by the user and therefore the actual number of frames is lower than initially planned
        num_existing_frames = num_frames
        for frame_index in range(num_frames):
            frame_key_key = 'FrameKey-%d-0-0' % (frame_index)
            if frame_key_key not in self.mmm.keys():
                # print('missing frame : %s' % frame_key_key)
                num_existing_frames = frame_index
                break
        return num_existing_frames

    @property
    def width(self):
        """Gets the width of images in the sequence
        """
        summary = self.mmm['Summary']
        return int(summary['Width'])

    @property
    def height(self):
        """Gets the height of images in the sequence
        """
        summary = self.mmm['Summary']
        return int(summary['Height'])

    @property
    def num_channels(self):
        """Gets the number of channels in the sequence
        """
        summary = self.mmm['Summary']
        return int(summary['Channels'])

    @property
    def num_slices(self):
        """Gets the number of slices in the sequence
        """
        summary = self.mmm['Summary']
        return int(summary['Slices'])

    @property
    def num_bits_per_pixels(self):
        summary = self.mmm['Summary']
        return int(summary['BitDepth'])

    def get_root_path(self):
        """Returns the root location of th is sequence
        """
        (dir_name, file_name) = os.path.split(self.micro_manager_metadata_file_path)  # pylint: disable=unused-variable
        return dir_name

    def get_image_file_path(self, channel_index, frame_index, slice_index=0):
        '''Returnsthe file location of the given image in the sequence
        :param int channel_index:
        :param int frame_index:
        :param int slice_index:
        '''
        assert frame_index < self.num_frames
        assert channel_index < self.num_channels
        try:
            frame_info = self.mmm['FrameKey-%d-%d-%d' % (frame_index, channel_index, slice_index)]
        except KeyError as e:
            raise MissingImageFile(sequence_id=self.id, frame_index=frame_index, slice_index=slice_index, channel_index=channel_index)
        rel_file_path = frame_info['FileName']
        return os.path.join(self.get_root_path(), rel_file_path)

    def get_channel_index(self, channel_id):
        '''
        :param str channel_id: the identifier of the channel (eg ``'vis'``)
        '''
        summary = self.mmm['Summary']
        channel_index = summary['ChNames'].index(channel_id)
        return channel_index

    def get_channel_name(self, channel_index):
        summary = self.mmm['Summary']
        return summary['ChNames'][channel_index]

    def get_channel_names(self):
        summary = self.mmm['Summary']
        return summary['ChNames']

    def get_black(self):
        ''' returns the black sequence related to the the sequence self

        :rtype: Sequence
        '''
        seqid_to_black = {
            'res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0': 'res_soleil2018/DARK/DARK_40X_60min_1 im pae min_1/Pos0',
            'res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos2': 'res_soleil2018/DARK/DARK_40X_60min_1 im pae min_1/Pos0',
        }
        white_sequence = seqid_to_black[self.id]
        return self.catalog.sequences[white_sequence]

    def get_white(self):
        ''' returns the white sequence related to the the sequence self

        :rtype: Sequence
        '''
        # assert fixme : res_soleil2018/white/white_24112018_2/Pos0 is visible
        seqid_to_white = {
            'res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0': 'res_soleil2018/white/white_24112018_2/Pos0',
            'res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos2': 'res_soleil2018/white/white_24112018_2/Pos0',
        }
        white_sequence = seqid_to_white[self.id]
        return self.catalog.sequences[white_sequence]

    def as_hyperstack(self, selected_channel_ids=None, selected_frames=None, selected_slices=None):
        """ returns a subset of the sequence as an hyperstack

        :param list(str) selected_channel_ids:
        :param list(int) selected_frames:
        :param list(int) selected_slices:
        :return: the resulting hyperstack
        :rtype: IHyperStack
        """
        if selected_frames is None:
            selected_frames = range(self.num_frames)

        if selected_slices is None:
            selected_slices = range(self.num_slices)

        if selected_channel_ids is None:
            selected_channel_indices = range(self.num_channels)
        else:
            selected_channel_indices = [self.get_channel_index(channel_id) for channel_id in selected_channel_ids]

        pixel_type = {8: PixelType.U8, 16: PixelType.U16}[self.num_bits_per_pixels]

        stack = IImageEngine.get_instance().create_hyperstack(width=self.width, height=self.height, num_slices=len(selected_slices), num_frames=len(selected_frames), num_channels=len(selected_channel_indices), pixel_type=pixel_type)
        for frame_index in selected_frames:
            for slice_index in selected_slices:
                for channel_index in selected_channel_indices:
                    try:
                        image_file_path = self.get_image_file_path(channel_index=channel_index, frame_index=frame_index, slice_index=slice_index)
                        image = IImageEngine.get_instance().load_image(image_file_path)
                        stack.set_image(image=image, channel_index=channel_index, frame_index=frame_index, slice_index=slice_index)
                    except MissingImageFile as e:
                        logger.warn('%s. As a result, the hyperstack will be black at this frame' % str(e))
                        # logger.warn('the image file for sequence "%s" frame %d, slice %d and channel %d is missing. As a result, the hyperstack will be black at this frame' % (self.id, frame_index, slice_index, channel_index))
        return stack

def find_dirs_containing_file(file_name, root_dir):
    """
    :param str file_name: the name of the searched files (eg 'metadata.txt')
    :param str root_dir: the root directory where we search
    :rtype: list(str)
    :return: the list of directories that contain the given file
    """
    result = []
    for root, dirs, files in os.walk(root_dir):  # pylint: disable=unused-variable
        if file_name in files:
            result.append(root)
    return result

class ImageCatalog(object):
    """A catalog of micromanager sequences
    """
    def __init__(self, raw_images_root):
        """Creates a new image catalog and populates it by finding the image sequences in the given root directory

        Args:
            raw_images_root (str): the root directory that is expected to contain the micromanager formatted sequences.

        """
        self.raw_images_root = raw_images_root
        self.sequences = {}
        """
        the dictionary which stores the catalog sequences (dict(str, :py:class:`Sequence`)). The sequences are indexed with the sequence identifiers.
        """

        sequence_paths = find_dirs_containing_file('metadata.txt', self.raw_images_root)
        logger.debug('sequence_paths : %s' % sequence_paths)
        sequence_ids = [ os.path.relpath(sequence_path, raw_images_root) for sequence_path in sequence_paths ]
        logger.debug('sequence_ids : %s' % sequence_ids)
        # nb : we use the path as sequence id because the "Comment" field in the summary section of the metadata file is not guaranteed to be unique (eg they are the same in res_soleil2018/white/white_24112018_1/Pos0 and in res_soleil2018/white/white_24112018_2/Pos0)

        for sequence_id in sequence_ids:
            if sequence_id == '.':
                # special degenerate case: when raw_images_root points to a Pos<n> directory, sequence_id is '.', which means there's no intemediate directories between raw_images_root and metadata.txt
                micro_manager_metadata_file_path = os.path.join(raw_images_root, 'metadata.txt')
            else:
                micro_manager_metadata_file_path = os.path.join(raw_images_root, sequence_id, 'metadata.txt')
            # micro_manager_metadata_file_path = '/tmp/toto.json'
            self.sequences[sequence_id] = Sequence(self, sequence_id, micro_manager_metadata_file_path)

    def __str__(self):
        for sequence_id, sequence in self.sequences.iteritems():
            return str(sequence_id) + ':' + str(sequence)
