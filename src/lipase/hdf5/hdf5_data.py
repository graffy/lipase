"""structures and classes for storing hdf5 data

see https://portal.hdfgroup.org/display/HDF5/Introduction+to+HDF5

note: these structures are intentionally made independent of h5py library as this h5py library is not available in all python environments. More specifically, h5py is not available in jython. So the purpose of this library is to allow storage of hdf5 data in all environments.
"""

class ElementType(object):
    """The types that can be used for elements of arrays
    """
    U1=0
    """1 bit boolean"""
    U8=1
    """8 bits unsigned integer"""
    U32=2
    """32 bits unsigned integer"""
    S32=3
    """32 bits signed integer"""
    F32=4
    """32 bits floating precision number"""
    F64=5
    """64 bits floating precision number"""

class DataSet(object):
    """ an hdf5 dataset    
    """
    def __init__(self, name, size, element_type=ElementType.U32):
        """
        :param str name: the name of the dataset
        :param tuple(int) size: the size of each dimension of the dataset
        :param ElementType element_type: the type of elements stored in this dataset
        """
        self.name = name
        """The name of the dataset (str)
        """
        self.size = size
        """The size of the dataset (tuple of integers)
        """
        self._element_type = element_type
        """The type of elements in this dataset (ElementType)
        """
        self._elements = []
        for i in range(self.num_elements):  # pylint: disable=unused-variable
            self._elements.append(0)

    @property
    def element_type(self):
        """Gets the element type of this array

        :rtype: ElementType
        """
        return self._element_type

    @property
    def dimension(self):
        """Gets the dimension of the array contained in this dataset

        Would return 3 for a 3D dataset.

        :rtype: int
        """
        return len(self.size)

    @property
    def elements(self):
        """Gets the elements value in the form of a list
        
        :rtype: list
        """
        return self._elements

    @property
    def num_elements(self):
        """Gets the number of elements in this dataset

        :rtype: int
        """
        num_elements = 1
        for i in range(self.dimension):
            num_elements *= self.size[i]
        return num_elements

    def get_element(self, pos):
        """Gets the value of the element indexed by pos

        :param tuple(int) pos: the index of the element to retreive. This index is expected to be a tuple of integers. The pos argument is expected to have the same length as the number of dimensions of this dataset.

        """
        assert len(pos) == len(self.size)
        return self._elements[self._pos_to_index(pos)]

    def __getitem__(self, pos):
        assert len(pos) == len(self.size)
        return self._elements[self._pos_to_index(pos)]

    def set_element(self, pos, value):
        """Sets the value of the element indexed by pos

        :param tuple(int) pos: the index of the element to set. This index is expected to be a tuple of integers. The pos argument is expected to have the same length as the number of dimensions of this dataset.
        :param value: the type is expected to be compatible (convertible to) with this dataset's element type
        """
        assert len(pos) == len(self.size)
        self._elements[self._pos_to_index(pos)] = value

    def __setitem__(self, pos, value):
        """Sets the value of the element indexed by pos

        :param tuple(int) pos: the index of the element to set. This index is expected to be a tuple of integers. The pos argument is expected to have the same length as the number of dimensions of this dataset.
        :param value: the type is expected to be compatible (convertible to) with this dataset's element type
        """
        assert len(pos) == len(self.size)
        self._elements[self._pos_to_index(pos)] = value

    def _pos_to_index(self, pos):
        index = 0
        for i in range(self.dimension):
            index += pos[i]
            if i < self.dimension - 1:
                index *= self.size[i]
        return index


class Group(object):
    """An hdf5 group

    A group can contain datasets and other groups
    """
    def __init__(self, name):
        """
        :param str name: the name of the group
        """
        self.name = name
        self._datasets = {}
        self._groups = {}

    def add_dataset(self, dataset):
        """Adds the given dataset to the datasets stored in this group

        :param DataSet dataset:
        """
        assert isinstance(dataset, DataSet)
        self._datasets[dataset.name] = dataset

    def add_group(self, group):
        """Adds the given group to the groups stored in this group

        :param Group group:
        """
        assert isinstance(group, Group)
        self._datasets[group.name] = group

    @property
    def datasets(self):
        """

        :rtype: dict(str, :py:class:`DataSet`)
        """
        return self._datasets

    @property
    def groups(self):
        """

        :rtype: dict(str, :py:class:`Group`)
        """
        return self._groups

    def __getitem__(self, dataset_name):
        return self._datasets[dataset_name]

