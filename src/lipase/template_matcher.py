from .imageengine import IImageEngine
from .maxima_finder import IMaximaFinder

class TemplateMatcher(object):
    """
    Method to detect a pattern in an image.

    This method finds the locations of the given pattern in the image, provided the pattern is replicated without other geometric distortions than translations.
    """

    def __init__(self, maxima_finder):
        """
        :param IMaximaFinder maxima_finder:
        """
        self.maxima_finder = maxima_finder

    def match_template(self, image, pattern):
        """Remove the traps in the input sequence

        :param IImage image: the image in which we want to detect the pattern
        :param IImage pattern:
        :return list(Matches): the list of detected matches
        """

        ie = IImageEngine.get_instance()

        correlation_image = ie.match_template(image, pattern)

        ie.debugger.on_image(correlation_image, 'similarity')

        matches = self.maxima_finder.find_maxima(correlation_image)
        return matches
        