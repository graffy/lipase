"""Lipase user settings."""
import json
#from os.path import expanduser
#import os.path
import os
from . import logger


class UserSettings(object):
    """User settings."""

    attributes = {'raw_images_root_path': {'type': unicode}}

    def __init__(self):
        """constructor."""
        self.settings = {}  # json dictionary
        self.raw_images_root_path = u'bla'
        try:
            with open(self.get_settings_file_path(), 'r') as json_file:
                self.settings = json.load(json_file)
        except IOError as error:
            logger.error('Failed to read %s : %s' % (self.get_settings_file_path(), error))

    def __getattr__(self, attr):
        """Override __getattr__ to check the validity of settings attributes names."""
        logger.debug('getting %s' % (attr,))
        if attr == 'settings':
            return self.__dict__['settings']
        else:
            assert attr in UserSettings.attributes
            return self.settings[attr]

    def __setattr__(self, attr, value):
        """Override __setattr__ to check the validity of settings attributes names."""
        logger.debug('setting %s to %s' % (attr, value))
        if attr == 'settings':
            self.__dict__['settings'] = value
        else:
            assert attr in UserSettings.attributes
            assert type(value) == UserSettings.attributes[attr]['type'], "invalid type for attribute %s : %s instead of %s" % (attr, str(type(value)), str(UserSettings.attributes[attr]['type']))
            self.settings[attr] = value

    def get_settings_file_path(self):
        """Return the location of lipase user settings file."""
        home = os.path.expanduser("~")
        return os.path.join(home, '.fr.univ-rennes1.ipr.lipase.json')

    def save(self):
        """Save user settings."""
        with open(self.get_settings_file_path(), 'w') as json_file:
            json.dump(self.settings, json_file)


def test():
    """Unit test."""
    settings = UserSettings()
    print(settings.raw_images_root_path)
    settings.raw_images_root_path = os.path.join(os.sep, 'Users', 'graffy', 'ownCloud', 'ipr', 'lipase', 'raw-images')
    # settings.toto = 'hello'
    settings.save()


if __name__ == '__main__':
    test()
