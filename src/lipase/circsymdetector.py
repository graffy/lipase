"""
    an image processing technique to produce a 1D signal for each pixel : this 1D signal is obtained by projecting the neighborhood of this pixel on a set of bases (each base is used as a convilution kernel)
    https://subversion.ipr.univ-rennes1.fr/repos/main/projects/antipode/src/python/antipode/texori.py
"""
from .imageengine import IImageEngine, PixelType, StackImageFeeder
import math

class IProjectorBase(object):
    """
        a set of projectors

        a projector is a grey level image, in which each pixel value is acting as a weight
    """
    def __init__(self):
        pass

    def get_num_projectors(self):
        pass
        
    def get_projector_kernel(self, projector_index):

        pass

def create_circle_image(image_size, circle_radius, circle_pos, circle_thickness, background_value=0.0, circle_value=1.0):
    """Creates an image containing the a circle

    :param dict image_size: The width and height of the image to create
    :rtype: IImage

    """
    ie = IImageEngine.get_instance()
    width = image_size['width']
    height = image_size['height']
    assert isinstance(width, int)
    assert isinstance(height, int)
    circle_pos_x = float(circle_pos['x'])
    circle_pos_y = float(circle_pos['y'])
    r_min = max(0.0, circle_radius - circle_thickness * 0.5)
    r_max = circle_radius + circle_thickness * 0.5
    r_min_square = r_min * r_min
    r_max_square = r_max * r_max
    image = ie.create_image(width=width, height=height, pixel_type=PixelType.F32)
    for y in range(height):
        for x in range(width):
            dx = x - circle_pos_x
            dy = y - circle_pos_y
            r_square = (dx * dx + dy * dy)
            if r_min_square - 0.001 <= r_square < r_max_square:
                pixel_value = circle_value
            else:
                pixel_value = background_value
            image.set_pixel(x, y, pixel_value)
    return image

def create_arc_image(image_size, circle_pos, radius_range, angle_range, background_value=0.0, circle_value=1.0):
    """
    :param dict image_size:
    """
    assert radius_range[0] < radius_range[1]
    assert angle_range[0] < angle_range[1]
    assert 0.0 <= angle_range[0] <= math.pi * 2.0
    assert 0.0 <= angle_range[1] <= math.pi * 2.0
    ie = IImageEngine.get_instance()
    width = image_size['width']
    height = image_size['height']
    assert isinstance(width, int)
    assert isinstance(height, int)
    circle_pos_x = float(circle_pos['x'])
    circle_pos_y = float(circle_pos['y'])
    r_min_square = radius_range[0] * radius_range[0]
    r_max_square = radius_range[1] * radius_range[1]
    image = ie.create_image(width=width, height=height, pixel_type=PixelType.F32)
    for y in range(height):
        for x in range(width):
            dx = x - circle_pos_x
            dy = y - circle_pos_y
            r_square = (dx * dx + dy * dy)
            pixel_value = background_value
            if r_min_square - 0.001 <= r_square < r_max_square:
                angle = math.atan2(dy, dx)
                if angle < 0.0:
                    angle += math.pi * 2.0
                assert 0.0 <= angle <= math.pi * 2.0, "unexpected value for angle : %f" % angle
                if angle_range[0] <= angle < angle_range[1]:
                    pixel_value = circle_value
                    # if 0.1 < angle < 0.2 and r_square > 2.0:
                    #    print("x", x, "y", y, "dx", dx, "dy", dy, "angle", angle)
                    #    assert False
            image.set_pixel(x, y, pixel_value)
    return image

class CircularSymmetryProjectorBase(object):
    """
        generates a base of circles arc kernels.
    """

    def __init__(self, max_radius, num_angular_sectors, num_radial_sectors, oversampling_scale=2):
        """
        :param int max_radius: the biggest circle radius in the set of projectors
        :param int num_angular_sectors: defines how many sectors we want in each circle
        :param int oversampling_scale: oversampling is used to generate antialased circles. The higher this value, the better the quality of antialiasing is
        """
        super(CircularSymmetryProjectorBase, self).__init__()
        assert max_radius > 0
        self.max_radius = max_radius
        self.num_radial_sectors = num_radial_sectors
        self.radial_step = self.max_radius / self.num_radial_sectors
 
        self.num_angular_sectors = num_angular_sectors
        self.angular_step = math.pi * 2.0 / self.num_angular_sectors
        assert oversampling_scale > 0
        self.oversampling_scale = oversampling_scale

    #def get_num_projectors(self):
    #    return self.num_projectors
        
    def get_projector_kernel(self, radius_index, angular_sector_index):
        """ Computes a kernel containing a patch in the shape of the arc of a circle.

        All patches together form a disc with a radius equal to max_radius.
        The arc corresponds to the the patch defined by radius_index and angular_sector_index

        :param int radius_index: index of the patch in the radial direction

        """
        assert radius_index < self.num_radial_sectors
        assert angular_sector_index < self.num_angular_sectors
        start_angle = angular_sector_index * self.angular_step
        end_angle = start_angle + self.angular_step
        oversampling_is_handled = False  # TODO: handle oversampling to increase quality
        if not oversampling_is_handled and self.oversampling_scale != 1  :
            raise NotImplementedError("at the moment, oversampling is not yet implemented")  
        radius_index
        image_size = int(self.max_radius) * 2 + 1
        circle_pos = {'x': self.max_radius, 'y': self.max_radius}
        oversampled_arc = create_arc_image(
            image_size={'width': image_size * self.oversampling_scale, 'height': image_size*self.oversampling_scale},
            circle_pos={'x': self.max_radius* self.oversampling_scale, 'y': self.max_radius* self.oversampling_scale},
            radius_range=(float(radius_index)* self.radial_step * self.oversampling_scale, float(radius_index + 1) * self.radial_step * self.oversampling_scale),
            angle_range=(start_angle, end_angle),)
        if self.oversampling_scale == 1:
            circle_image = oversampled_arc
        else:
            if oversampling_is_handled:
                circle_image = oversampled_arc.resample(width=image_size, height=image_size)
        mean_value = circle_image.get_mean_value()
        assert mean_value > 0.0, "unexpected mean value of this arc image : this circle might be empty, or worse, have negative values, which doesn't make sense"
        num_pixels = image_size * image_size
        sum_of_pixel_values = mean_value * num_pixels
        # we want each circle to have a total weight of 1.0, regardless their radius
        circle_image.scale_values(1.0/sum_of_pixel_values)
        
        anchor_point = {'x': int(circle_pos['x']), 'y': int(circle_pos['y'])}
        return circle_image, anchor_point


class CircularSymmetryDetector:
    """Method to detect objects exhibiting a circular symmetry
    """
    def __init__(self, max_radius, num_angular_sectors, num_radial_sectors=None):
        """Constructor

        Parameters
        ----------
        max_radius : float
            in pixels
        num_angular_sectors : int
            the angle range [0;2 pi] is sampled into num_angular_sectors
        num_radial_sectors : int or None
            the radius range [0;max_radius] is sampled into num_radial_sectors. If not defined, the number of radial sectors is set so that a radial sector has the width of 1 pixel
        """
        self.max_radius = max_radius
        self.num_angular_sectors = num_angular_sectors
        if num_radial_sectors is None:
            num_radial_sectors = int(max_radius)
        self.num_radial_sectors = num_radial_sectors

    def compute_radial_profiles(self, src_image):
        """ Computes for each pixel the radial profile (with this pixel as center)

        :param IImage src_image:
        :returns:
            - radial_profiles (:py:class:`~lipase.imageengine.IHyperStack`) - each pixel stores the radial profile for this pixel. The radial profile is a 1D signal stored along the channel axis of the hyperstack, each channel being related to a radius range.
            - angular_stddev_profiles (:py:class:`IHyperStack`) - each pixel stores the angular variance profile for this pixel. The angular variance profile is a 1D signal stored along the channel axis of the hyperstack, each channel being related to a radius range.
        """
        # https://stackoverflow.com/questions/39759503/how-to-document-multiple-return-values-using-restructuredtext-in-python-2
        ie = IImageEngine.get_instance()
        ie.debugger.on_image(src_image, 'src_image')
        projector_base = CircularSymmetryProjectorBase(self.max_radius, num_angular_sectors=self.num_angular_sectors, num_radial_sectors=self.num_radial_sectors, oversampling_scale=1)

        radial_profiles = ie.create_hyperstack(width=src_image.get_width(), height=src_image.get_height(), num_channels=projector_base.num_radial_sectors, num_slices=1, num_frames=1, pixel_type=PixelType.F32)
        angular_stddev_profiles = ie.create_hyperstack(width=src_image.get_width(), height=src_image.get_height(), num_channels=projector_base.num_radial_sectors, num_slices=1, num_frames=1, pixel_type=PixelType.F32)
        for radius_index in range(projector_base.num_radial_sectors):
            circle_stack = ie.create_hyperstack(width=src_image.get_width(), height=src_image.get_height(), num_channels=projector_base.num_angular_sectors, num_slices=1, num_frames=1, pixel_type=PixelType.F32)
            for angular_sector_index in range(projector_base.num_angular_sectors):
                arc_kernel, center_of_filter = projector_base.get_projector_kernel(radius_index, angular_sector_index)
                ie.debugger.on_image(arc_kernel, 'projector_%d_%d' % (radius_index, angular_sector_index))
                projection = ie.filter2D(src_image, dst_type=PixelType.F32, kernel=arc_kernel, anchor=(center_of_filter['x'], center_of_filter['y']))
                # ie.debugger.on_image(projection, 'projection_%d' % (angular_sector_index))
                circle_stack.set_image(projection, frame_index=0, slice_index=0, channel_index=angular_sector_index)
            # compute the image in which each pixel contains the mean value of the source image in the given circular region around the pixel
            circle_mean_image = ie.compute_mean(StackImageFeeder(circle_stack))
            radial_profiles.set_image(circle_mean_image, frame_index=0, slice_index=0, channel_index=radius_index)

            angular_stddev_image = ie.compute_stddev(StackImageFeeder(circle_stack))
            angular_stddev_profiles.set_image(angular_stddev_image, frame_index=0, slice_index=0, channel_index=radius_index)
        return radial_profiles, angular_stddev_profiles

    def compute_stddev_images(self, src_image):
        """Computes the radial variance image and the angular variance average image

        Parameters
        ----------
        src_image: IImage
            the source image

        Returns
        -------
        radial_stddev_image : IImage
            Each pixel contains the standard deviation of the radial profile for this pixel
        angular_stddev_avg_image : IImage
            Each pixel contains the average of standard deviation for each circle radius
        """
        ie = IImageEngine.get_instance()
        radial_profiles, angular_stddev_profiles = self.compute_radial_profiles(src_image)
        radial_stddev_image = ie.compute_stddev(StackImageFeeder(radial_profiles))

        angular_stddev_avg_image = ie.compute_mean(StackImageFeeder(angular_stddev_profiles))
        return radial_stddev_image, angular_stddev_avg_image
        

class GlobulesDetector(object):

    def __init__(self, circ_sym_filter, circles_finder, ignore_mask=None):
        """Constructor

        Parameters
        ----------
        circ_sym_filter : CircularSymmetryDetector
            the method and its parameters used to compute the radial and angular profile images
        circles_finder : IMaximaFinder
            the method to detect circles from the circularness image
        ignore_mask : IImage or None
            the pixels that can't have globules
        """
        self.circ_sym_filter = circ_sym_filter
        self.circles_finder = circles_finder
        self.ignore_mask = ignore_mask

    
    def detect_globules(self, src_image):
        """Detects the globules in the given input image

        Parameters
        ----------
        src_image : IImage
            the input image

        """
        ie = IImageEngine.get_instance()
        radial_profiles, angular_stddev_profiles = self.circ_sym_filter.compute_radial_profiles(src_image)
        ie.debugger.on_hyperstack(radial_profiles, 'radial_profiles')
        ie.debugger.on_hyperstack(angular_stddev_profiles, 'angular_stddev_profiles')

        radial_stddev_image = ie.compute_stddev(StackImageFeeder(radial_profiles))
        ie.debugger.on_image(radial_stddev_image, 'radial_stddev_image')

        angular_stddev_avg_image = ie.compute_mean(StackImageFeeder(angular_stddev_profiles))
        ie.debugger.on_image(angular_stddev_avg_image, 'angular_stddev_avg_image')
        circularness = ie.subtract(radial_stddev_image, angular_stddev_avg_image)
        if self.ignore_mask:
            circularness.set_masked_pixels(self.ignore_mask, 0.0)
        
        ie.debugger.on_image(circularness, 'circularness')
        matches = self.circles_finder.find_maxima(circularness)
        print('number of globules found : %d' % len(matches))
        for match in matches:
            print(match)
        return matches



