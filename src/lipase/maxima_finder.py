import abc
from .imageengine import IImageEngine

ABC = abc.ABCMeta('ABC', (object,), {})

class Match(object):

    def __init__(self, x, y, correlation):
        self.x = x
        self.y = y
        self.correlation = correlation

    def __str__(self):
        return "(%d, %d) - %f" % (self.x, self.y, self.correlation)

    def __repr__(self):
        return self.__str__()

class IMaximaFinder(ABC):
    """Abstract class for methods aiming at detecting the maxima of an image
    """

    @abc.abstractmethod
    def find_maxima(self, image):
        """
        :param IImage image:
        :rtype: list(Match)
        """

class MaximaFinder(IMaximaFinder):
    """
    A maxima finder based on imagej's MaximumFinder

    Uses the technique implemented in https://github.com/imagej/imagej1/blob/master/ij/plugin/filter/MaximumFinder.java
    """

    def __init__(self, threshold, tolerance):
        """
        to understand the meaning of threshold and tolerance, I had to look at the source code in https://github.com/imagej/imagej1/blob/master/ij/plugin/filter/MaximumFinder.java#L636
        1. The algorithm first builds the list of maxima candidates. Only local maxima above this threshold are considered as valid maxima candidates
        2. Then each maximum candidate is processed to find its neighborhood area, starting from the highest. For ecach candidate, a propagation is performed on neighbours as long as the neighbour value is within the tolerance of the maximum candidate. If another candidate is encountered as a neighbour, then this candidate is removed as it('s been absorbed by the current maximum candidate)

        :param float threshold: local maxima below this value are ignored
        :param float tolerance: ignore local maxima if they are in the neighborhood of another maximum. The neighborhood of a maximum is defined by the area that propagates until the the difference between the pixel value and the value of the considered local maximum exceeds tolerance. The higher the tolerance, the more local maxima are removed.... 
        """
        self.threshold = threshold
        self.tolerance = tolerance

    def find_maxima(self, correlation_image):
        ie = IImageEngine.get_instance()
        matches = ie.find_maxima(correlation_image, self.threshold, self.tolerance)
        return matches

