"""An image engine implements image processing operators and an abstract image format.

Using these abstract classes allows the user to write image processing code that can run with various different image engines (eg imagej or opencv).
"""


import abc
import errno
import os

ABC = abc.ABCMeta('ABC', (object,), {})

class Aabb(object):
    """Axis-Aligned bounding box"""

    def __init__(self, x_min, y_min, x_max, y_max):
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max

    @property
    def width(self):
        return self.x_max - self.x_min + 1
        
    @property
    def height(self):
        return self.y_max - self.y_min + 1

class PixelType:
    U8 = 1
    U16 = 2
    F32 = 3

    PIXEL_TYPE_TO_NUM_BITS = {U8: 8,
        U16: 16,
        F32:32}

    @staticmethod
    def get_num_bits(pixel_type):
        return PixelType.PIXEL_TYPE_TO_NUM_BITS[ pixel_type ]

class IImage(ABC):
    """An abstact class representing an image (2D matrix of pixels)

    """
    @abc.abstractmethod
    def clone(self, clone_pixel_type=None):
        """
        :param int clone_pixel_type: PixelType enum. If None, then the clone's pixel type is the same as the pixel type of the source image
        """
        pass

    @abc.abstractmethod
    def get_width(self):
        pass

    @abc.abstractmethod
    def get_height(self):
        pass

    @abc.abstractmethod
    def set_pixel(self, x, y, value):
        pass

    @abc.abstractmethod
    def get_subimage(self, aabb):
        """
        :param Aabb aabb: axis-aligned bounding box of the subimage
        :return IImage:
        """
        pass

    @abc.abstractmethod
    def set_subimage(self, subimage, position):
        """
        :param IImage subimage: the image to copy into self
        :param (int,int) position: where to insert the subimage in self (upper left pixel)
        """

    @abc.abstractmethod
    def get_pixel_type(self):
        """
        :rtype: PixelType
        """
        pass

    @abc.abstractmethod
    def get_value_range(self):
        """
        :rtype: (float, float)
        :returns: the min and the max value
        """
        pass
    
    @abc.abstractmethod
    def get_mean_value(self):
        """
        :rtype float:
        :return: the mean pixel value in the image
        """
        pass

    @abc.abstractmethod
    def scale_values(self, src_image, scale):
        """
        multiply the value of each pixel by a the given scalar

        :param float scale:
        """

    @abc.abstractmethod
    def set_masked_pixels(self, mask, pixel_value):
        """
        Sets the value of pixels (for which the related mask value is non-zero) to pixel_value

        Parameters
        ----------

        mask : IImage
            only pixels for which mask value is non zero are modified
        pixel_value : float
            the new pixel value for modified pixels
        """
    


class IHyperStack(ABC):
    """An abstact class representing an hyperstack (5D matrix of pixels)

    The name hyperstachk comes from imagej's terminology.

    """

    @abc.abstractmethod
    def get_width(self):
        pass

    @abc.abstractmethod
    def get_height(self):
        pass

    @abc.abstractmethod
    def num_slices(self):
        pass

    @abc.abstractmethod
    def num_frames(self):
        pass

    @abc.abstractmethod
    def num_channels(self):
        pass


    @abc.abstractmethod
    def set_image(self, image, frame_index=0, slice_index=0, channel_index=0):
        """
        :param IImage image:
        """
        pass

    @abc.abstractmethod
    def get_image(self, frame_index=0, slice_index=0, channel_index=0):
        """
        :param IImage image:
        """
        pass


class IImageFeeder(ABC):
    """An object that generates a collection of images of the same size and same format
    """

    @abc.abstractmethod
    def __iter__(self):
        """
        for iterator
        """
        pass 

    @abc.abstractmethod
    def next(self):
        """returns the next image in the collection
        for iterator

        :rtype: IImage
        """
        pass

    @abc.abstractmethod
    def get_num_images(self):
        """Gets the number of images in this collection
        """
        pass

    def create_hyperstack(self):
        """
        creates an hyperstack from this image feeder

        :rtype: IHyperStack
        """
        it = iter(self)

        image = it.next()
        ie = IImageEngine.get_instance()
        hyperstack = ie.create_hyperstack(width=image.get_width(), height=image.get_height(), num_channels=1, num_slices=1, num_frames=self.get_num_images(), pixel_type=image.get_pixel_type())
        frame_index = 0
        for image in it:
            hyperstack.set_image(image, frame_index=frame_index)
            frame_index += 1
        # print(frame_index)
        # print(self.get_num_images())
        
        assert frame_index == self.get_num_images()
        return hyperstack

class FileImageFeeder(IImageFeeder):

    def __init__(self):
        self.image_filepaths = []
        self.next_image_index = 0

    def __iter__(self):
        self.next_image_index = 0
        return self

    def next(self):
        if self.next_image_index < len(self.image_filepaths):
            image_filepath = self.image_filepaths[self.next_image_index]
            self.next_image_index += 1
            image = IImageEngine.get_instance().load_image(image_filepath)
            return image
        raise StopIteration

    def get_num_images(self):
        return len(self.image_filepaths)

    def add_image(self, image_filepath):
        self.image_filepaths.append(image_filepath)


class StackImageFeeder(IImageFeeder):

    def __init__(self, hyperstack):
        """
        :param IHyperStack hyperstack:
        """
        self.hyperstack = hyperstack

    def _init_iter(self):
        self.next_frame_index = 0
        self.next_slice_index = 0
        self.next_channel_index = 0
        self.end_is_reached = False

    def __iter__(self):
        self._init_iter()
        return self

    def next(self):
        # print("channel %d/%d frame %d/%d slice %d/%d" % (self.next_channel_index, self.hyperstack.num_channels(), self.next_frame_index, self.hyperstack.num_frames(), self.next_slice_index, self.hyperstack.num_slices()))
        if self.end_is_reached:
            raise StopIteration
        else:
            image = self.hyperstack.get_image(frame_index=self.next_frame_index, slice_index=self.next_slice_index, channel_index=self.next_channel_index)
            # mean_value = image.get_mean_value()
            # print("mean_value", mean_value)
            # assert mean_value > 0.001
        
        # compute next image index
        self.next_slice_index += 1
        if self.next_slice_index == self.hyperstack.num_slices():
            self.next_slice_index = 0
            self.next_frame_index += 1
            if self.next_frame_index == self.hyperstack.num_frames():
                self.next_frame_index = 0
                self.next_channel_index += 1
                if self.next_channel_index == self.hyperstack.num_channels():
                    self.end_is_reached = True


        # if self.next_slice_index < self.hyperstack.num_slices() - 1:
        #     self.next_slice_index += 1
        # else:
        #     self.next_slice_index = 0
        #     if self.next_frame_index < self.hyperstack.num_frames() - 1:
        #         self.next_frame_index += 1
        #     else:
        #         self.next_frame_index = 0
        #         if self.next_channel_index < self.hyperstack.num_channels():
        #             self.next_channel_index += 1
        #         else:
        #             self.end_is_reached = True
        # print("after : channel %d/%d frame %d/%d slice %d/%d" % (self.next_channel_index, self.hyperstack.num_channels(), self.next_frame_index, self.hyperstack.num_frames(), self.next_slice_index, self.hyperstack.num_slices()))
        return image

    def get_num_images(self):
        return self.hyperstack.num_frames() * self.hyperstack.num_slices() * self.hyperstack.num_channels()


class IImageProcessingDebugger(ABC):
    """
    An abstract image processing debugger.

    Its role is to do a specific action on some events related to image processing
    """

    def __init__(self):
        pass

    @abc.abstractmethod
    def on_image(self, image, image_id):
        """This method is called by image processing methods when they have an intermediate image they want to send to the debugger
        
        Parameters
        ----------
        image : IImage
            the image sent to the debugger
        image_id : str
            an identifier for the given image, which is used by the debugger as it wishes (to act on a specific image, or as a filename if the debugger saves the images it receives, etc.)
        """

    @abc.abstractmethod
    def on_hyperstack(self, hyperstack, hyperstack_id):
        """This method is called by image processing methods when they have an intermediate hyperstack they want to send to the debugger (to act on a specific image, or as a filename if the debugger saves the hyperstack it receives, etc.)

        Parameters
        ----------
        hyperstack : IHyperstack
            the hyperstack sent to the debugger
        hyperstack_id : str
            an identifier for the given hyperstack
        """

class NullDebugger(IImageProcessingDebugger):
    """
    This debugger is simply ignoring anything it receives.

    Therefore this debugger is to be chosen when we want to disable the debugger.
    """
    def __init__(self):
        IImageProcessingDebugger.__init__(self)

    def on_image(self, image, image_id):
        pass  # do nothing

    def on_hyperstack(self, hyperstack, hyperstack_id):
        pass  # do nothing

def mkdir_p(path):
    if os.path.isdir(path) :
        return
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >= 2.5
        already_exists_error_code = 20000  # normally it's errno.EEXIST (17) but for some reason it's 20000 in jython
        if exc.errno == already_exists_error_code and os.path.isdir(path):
            pass
        else:
            raise


class FileBasedDebugger(IImageProcessingDebugger):


    def __init__(self, debug_images_root_path='/tmp'):
        IImageProcessingDebugger.__init__(self)
        self.debug_images_root_path = debug_images_root_path
        mkdir_p(self.debug_images_root_path)

    def on_image(self, image, image_id):
        # print('FileBasedDebugger.on_image : image_id = %s' % image_id)
        ie = IImageEngine.get_instance()
        ie.save_image_as_tiff(image, '%s/%s.tiff' % (self.debug_images_root_path, image_id))

    def on_hyperstack(self, hyperstack, hyperstack_id):
        # print('FileBasedDebugger.on_image : image_id = %s' % image_id)
        ie = IImageEngine.get_instance()
        ie.save_hyperstack_as_tiff(hyperstack, '%s/%s.tiff' % (self.debug_images_root_path, hyperstack_id))


class IImageEngine(ABC):
    """
    The active image processor.

    As there should only be one image engine instance at any given time, this class is implemented as a singleton
    """

    # Here will be the instance stored.
    __instance = None

    @staticmethod
    def set_instance(image_engine):
        """Change the active image_engine.

        :param IImageEngine image_engine:
        """
        IImageEngine.__instance = image_engine

    @staticmethod
    def get_instance():
        """Static access method.
        :rtype: IImageEngine
        """

        assert IImageEngine.__instance is not None
        return IImageEngine.__instance

    def __init__(self, debugger=NullDebugger()):
        """
        :param IImageProcessingDebugger debugger:
        """
        self.debugger = debugger

    @abc.abstractmethod
    def create_hyperstack(self, width, height, num_channels, num_slices, num_frames, pixel_type):
        """
        :rtype: IHyperStack
        """

    @abc.abstractmethod
    def create_image(self, width, height, pixel_type):
        """
        :rtype: IImage
        """

    @abc.abstractmethod
    def load_image(self, src_image_file_path):
        """
        :param str src_image_file_path: eg './white_estimate.tiff'
        :return IImage:
        """

    @abc.abstractmethod
    def save_image_as_tiff(self, image, out_file_path):
        """
        :param IImage image:
        :param str out_file_path: eg './white_estimate.tiff'
        """

    @abc.abstractmethod
    def subtract(self, image1, image2):
        """
        computes the difference image1-image2

        :param IImage image1:
        :param IImage image2:
        :rtype: IImage
        :return: image1-image2
        """

    @abc.abstractmethod
    def multiply(self, image1, image2):
        """
        computes the difference image1-image2

        :param IImage image1:
        :param IImage image2:
        :rtype: IImage
        :return: image1*image2
        """

    @abc.abstractmethod
    def divide(self, numerator_image, denominator_image):
        """
        computes the division numerator_image/denominator_image

        :param IImage numerator_image:
        :param IImage denominator_image:
        :rtype: IImage
        :return: numerator_image/denominator_image
        """

    @abc.abstractmethod
    def abs(self, image):
        """
        computes the absolute value of each pixel in the input image

        :param IImage image:
        :rtype: IImage
        :return: an image containing the absolute values of the input image
        """


    @abc.abstractmethod
    def compute_edge_transform(self, image):
        """
        computes the edge transform using a sobel filter

        :param IImage image:
        :rtype: IImage
        """

    @abc.abstractmethod
    def compute_max(self, image_feeder):
        """Compute for each pixel position the maximum at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IImage
        """

    @abc.abstractmethod
    def compute_median(self, image_feeder):
        """Compute for each pixel position the median value at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IImage
        """

    @abc.abstractmethod
    def compute_mean(self, image_feeder):
        """Compute for each pixel position the mean value at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IImage
        """

    @abc.abstractmethod
    def compute_stddev(self, image_feeder):
        """Compute for each pixel position the standard deviation at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IImage
        """

    @abc.abstractmethod
    def compute_variance(self, image_feeder):
        """Compute for each pixel position the variance at this position in all input images.

        :param IImageFeeder image_feeder:
        :rtype: IImage
        """

    @abc.abstractmethod
    def mean_filter(self, image, radius):
        """Each pixel becomes an average of its neighbours within the given radius

        :param IImage image:
        :param int radius: in pixels
        """

    @abc.abstractmethod
    def filter2D(self, src_image, dst_type, kernel, anchor=(-1, -1)):
        """ as cv2.filter2D

        :param IImage src_image:
        :param PixelType or None dst_type: pixel type of the destination image (if None, the pixel type is the same as the source image type)
        :param IImage kernel: convolution kernel (or rather a correlation kernel), a single-channel floating point matrix; if you want to apply different kernels to different channels, split the image into separate color planes using split() and process them individually.
        :param tuple(int, int) angor: anchor of the kernel that indicates the relative position of a filtered point within the kernel; the anchor should lie within the kernel; default value (-1,-1) means that the anchor is at the kernel center. 
        :rtype: IImage
        """

    @abc.abstractmethod
    def perform_gray_morphology(self, image, operator, structuring_element_shape, structuring_element_radius):
        """
        :param IImage image:
        :param str operator: eg 'open'
        :param str structuring_element_shape: eg 'square'
        :param int structuring_element_radius:
        """

    @abc.abstractmethod
    def replace_border(self, image, band_width):
        """Overwrites the outer band of the image by duplicating the value of the pixels that touch this outer band

        :param IImage image:
        :param int band_width: width of the outer band, in pixels
        :rtype: IImage

        adaptation for the following code from matlab telemosToolbx's::

            estimatedwhiteFluoImageTelemos function
            outer = white_estimate;
            white_estimate = white_estimate(4:(end-3),4:(end-3));
            outer(1:3,4:(end-3))=repmat(white_estimate(1,:),3,1);  # top
            outer((end-2):end,4:(end-3))=repmat(white_estimate(end,:),3,1); # bottom
            outer(:,1:3)=repmat(outer(:,4),1,3); # left
            outer(:,(end-2):end)=repmat(outer(:,end-3),1,3); # right
            white_estimate = outer;
            clear outer;
        """

    @abc.abstractmethod
    def match_template(self, src_image, template_image):
        """
        :param IImage src_image:
        :param IImage template_image:
        :rtype: IImage
        :return: similarity image
        """

    @abc.abstractmethod
    def find_maxima(self, src_image, threshold, tolerance):
        """
        to understand the meaning of threshold and maxima, I had to look at the source code in https://github.com/imagej/imagej1/blob/master/ij/plugin/filter/MaximumFinder.java#L636

        1. The algorithm first builds the list of maxima candidates. Only local maxima above this threshold are considered as valid maxima candidates
        2. Then each maximum candidate is processed to find its neighborhood area, starting from the highest. For ecach candidate, a propagation is performed on neighbours as long as the neighbour value is within the tolerance of the maximum candidate. If another candidate is encountered as a neighbour, then this candidate is removed as it('s been absorbed by the current maximum candidate)

        :param IImage src_image:
        :param float threshold: local maxima below this value are ignored
        :param float tolerance: ignore local maxima if they are in the neighborhood of another maximum. The neighborhood of a maximum is defined by the area that propagates until the the difference between the pixel value and the value of the considered local maximum exceeds tolerance. The higher the tolerance, the more local maxima are removed.... 
        :return: maxima
        :rtype: list(Match)
        """

    @abc.abstractmethod
    def threshold(self, src_image, threshold_value):
        """
        :param float threshold_value:
        :rtype: IImage
        :return: the resulting image
        """

    @abc.abstractmethod
    def auto_threshold(self, src_image):
        """
        :rtype: IImage 
        """

    @abc.abstractmethod
    def flood_fill(self, src_binary_image, flood_start_pos):
        """
        performs flood fill from starting point

        :param IImage src_binary_image:
        :param (int, int) flood_start_pos: start position of the flood fill
        :rtype: IImage
        :return: binary image
        """

    @abc.abstractmethod
    def fill_holes(self, src_binary_image):
        """
        fills the holes in the given binary image

        :param IImage src_binary_image:
        :rtype: IImage
        :return: binary image
        """

    @abc.abstractmethod
    def invert(self, src_binary_image):
        """
        inverts the given image pixel values (black becomes white and white becomes black)

        :param IImage src_binary_image:
        :rtype: IImage
        :return: binary image
        """

