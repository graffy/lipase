"""preprocessing of synchrotron images based on telemosToolbox."""

from .catalog import ImageCatalog, Sequence
from .imageengine import IImageEngine, PixelType, FileImageFeeder

class WhiteEstimator(object):
    """
    Method that estimates the white from a sequence of images

    This method expects the sequence of images to represen sparse particles moving over a white background

    Code adapted from matlab telemosToolbx's estimatewhiteFluoImageTelemos function::

        % this function is specific of Telemos images (DISCO line SOLEIL)
            % acquired using micromanager software linked to imageJ

        %% input

        %  nothing : interactive function

        %% output

        %  nothing : the final reference fluorescence image is saved on disk

        %% principe
        %  NB: MICROMANAGER save images in a structured file folder architecture :
        %
        % root folder : name given by the user
        %
        %   subfolders roi(n)_tile1 : n folders for each selected roi
        %           or pos(n) : n folders for each selected position
        %
        %   display_and_comments.txt : file describing the channels acquired
        %
        % in each subfolder roi(n)_tile1 or Pos(n) :
        %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
        %               n = number of time
        %               p = z focal plane
        %               DM300_327-353 = name of the channel
        %       metadata.txt : files describing all metadata associated with the
        %       acquisition : x, y, z position, camera settings .... etc.
        %
        % (Nb: depending on the date of acquisition, an extension fluo is found or
        % not in the name of fluorescence image. This extension is given in
        % contrast to the visible image.)
        %
        % IMAGES : fluorescence images are considered
        %
        %
        % IMPORTANT:
        %       it is expected that in the folders at least several images cover the whole field of view
        %       illuminated by the synchrtron light
        %       IF IT IS NOT THE CASE, IT WON'T WORK
        %
        % BASIC: the estimated white image is the MAX of the selected images
        %       when interactive mode is selected, only first time and first z are
        %       proposed
        %       for automatic computing, all images are taken into account
        %
        % WHAT IS DONE:
        %        0 - the user chosse the channels that are considered  in the estimation of the
        % white fluorescence image
        %
        %       1 - spurious pixels (three lines and columns around the image) are not taken
        % in filtering and replaced by line and column number 4 and end-3
        %
        %       2 - Filtering
        % the estimated dark image is filtered :
        %        opening to remove  white regions brighter than the low frequency signal that should correspond to  synchrotron light shape (should be small)
        %        closing to remove black regions that remains (should be small to avoid synchrotron light shape deformation)
        %        average filtering

        % the image computed should be used  to
        %   - to find the best white z plane from the reference white stack
        %   acquired for ex. using the so called "Matthieu lame"%
        %   - correct images for intensities

        %% use
        %       estimatedwhiteFluoImageTelemos

        %% Comments
        %   adapted from proposals 20161050 and 20171187


        %% Author
        % MF Devaux
        % INRA BIA
        % PVPP

        %% date
        % 5 octobre 2017:
        % 23 mars 2018
        % 3 septembre 2018 : comments and general case
        % 16 avril 2019: name of function and default filtering values
        % 29 avril 2019 : new comments and spurious pixels
        % 27 mai 2019 : offset dark
        %     4 juin 2019 : replace exist by isfolder or isfile
        % 14 juin : close figure at the end
    """

    def __init__(self, open_size, close_size, average_size):
        """
        :param int open_size: the diameter of the structuring element used to perform mathematical morphology's opening operator (in pixels)
        :param int close_size: the diameter of the structuring element used to perform mathematical morphology's closing operator (in pixels)
        """
        self.open_size = open_size
        self.close_size = close_size
        self.average_size = average_size

    def _remove_particles(self, white_estimate):
        IImageEngine.get_instance().perform_gray_morphology(white_estimate, operator='open', structuring_element_shape='square', structuring_element_radius=(self.open_size + 1) / 2)
        IImageEngine.get_instance().perform_gray_morphology(white_estimate, operator='close', structuring_element_shape='square', structuring_element_radius=(self.close_size + 1) / 2)

        IImageEngine.get_instance().mean_filter(white_estimate, radius=(self.average_size + 1) / 2)

    def estimate_white(self, sequences, channel_ids, dark=None):
        """Estimation of the white fluorescence image shape of synchrotron light from experimental images of Telemos microscope.

        :param list(Sequence) sequences: the sequences to consider
        :param list(str) channel_ids: the channels to consider, eg ['DM300_327-353_fluo']
        :param ImagePlus or None dark:
        :rtype: ImagePlus
        """        
        image_set = FileImageFeeder()
        for sequence in sequences:
            for channel_id in channel_ids:
                channel_index = sequence.get_channel_index(channel_id)
                for frame_index in range(sequence.num_frames):
                    for slice_index in range(sequence.num_slices):
                        image_set.add_image(sequence.get_image_file_path(channel_index, frame_index, slice_index))

        white_estimate = self.estimate_white_from_image_set(image_set, dark)
        return white_estimate

    def estimate_white_from_image_set(self, image_set, dark=None):
        """Estimation of the white fluorescence image shape of synchrotron light from experimental images of Telemos microscope.

        :param IImageFeeder image_set: the set of input images to consider
        :param WhiteEstimatorSettings white_estimator_settings: 

        """
        white_estimate = IImageEngine.get_instance().compute_max(image_set)
        
        # modify spurious pixels on the side of the images
        try:
            IImageEngine.get_instance().replace_border(white_estimate, 3)
        except NotImplementedError as error:  # pylint: disable=unused-variable
            print('warning: replace_outer_frame is not implemented yet')

        self._remove_particles(white_estimate)
        
        return white_estimate


class InteractiveWhiteEstimator(WhiteEstimator):

    def __init__(self, open_size, close_size, average_size):
        WhiteEstimator.__init__(self, open_size, close_size, average_size)

    def _remove_particles(self, white):
        """shows the image to a user so that he can visually check that the particles have been removed correctly in the given image
        """
        raise NotImplementedError()
        # part_rem_settings_are_good = False
        # while part_rem_settings_are_good == False:
        #     # perform opening to remove white particles
        #     IJ.run( white_estimate, "Gray Morphology", "radius=1 type=square operator=open" )
        #     # run("Gray Morphology", "radius=1 type=square operator=open");
        #     # IJ.run( input_image_plus_copy, "Skeletonize (2D/3D)", "" )
        #     # perform 
        #     if white_estimator_settings.particles_are_removed():
        #         part_rem_settings_are_good = true
        #     else
        #         white_estimator_settings.ask


def correct_non_uniform_lighting(non_uniform_sequence, channel_id, white_estimator=WhiteEstimator(open_size=75, close_size=75, average_size=75)):
    """ correction of non uniform lighting

    :param Sequence non_uniform_sequence: the input sequence, which is expected to have a non uniform lighting
    :param str channel_id:
    :param WhiteEstimator white_estimator: the method used to estimate the light image
    :rtype: IHyperStack
    :return: the hyperstack of images after correction
    """
    ie = IImageEngine.get_instance()

    white_estimate = white_estimator.estimate_white([non_uniform_sequence], [channel_id])
    uniform_stack = ie.create_hyperstack(width=white_estimate.get_width(), height=white_estimate.get_height(), num_slices=1, num_frames=non_uniform_sequence.num_frames, num_channels=1, pixel_type=PixelType.F32)

    for frame_index in range(non_uniform_sequence.num_frames):
        non_uniform_frame = ie.load_image(non_uniform_sequence.get_image_file_path(channel_index=0, frame_index=frame_index, slice_index=0))
        uniform_frame = ie.divide(non_uniform_frame, white_estimate)
        uniform_stack.set_image(frame_index=frame_index, image=uniform_frame)
    return uniform_stack


# def get_sequence_as_hyperstack(sequence, selected_channel_ids=None, selected_frames=None, selected_slices=None):
#     """ returns a subset of the sequence as an hyperstack
#     :param IHyperStack non_uniform_sequence: the input sequence
#     :param list(str) selected_channel_ids:
#     :return IHyperStack: the resulting hyperstack
#     """
#     if selected_frames is None:
#         selected_frames = range(sequence.num_frames)

#     if selected_slices is None:
#         selected_slices = range(sequence.num_slices)

#     if selected_channel_ids is None:
#         selected_channel_indices = range(sequence.num_channels)
#     else:
#         selected_channel_indices = [sequence.get_channel_index(channel_id) for channel_id in selected_channel_ids]

#     pixel_type = {8: PixelType.U8, 16: PixelType.U16}[sequence.num_bits_per_pixels]

#     stack = IImageEngine.get_instance().create_hyperstack(width=sequence.width, height=sequence.height, num_slices=len(selected_slices), num_frames=len(selected_frames), num_channels=len(selected_channel_indices), pixel_type=pixel_type)
#     for frame_index in selected_frames:
#         for slice_index in selected_slices:
#             for channel_index in selected_channel_indices:
#                 image = IImageEngine.get_instance().load_image(sequence.get_image_file_path(channel_index=channel_index, frame_index=frame_index, slice_index=slice_index))
#                 stack.set_image(image=image, channel_index=channel_index, frame_index=frame_index, slice_index=slice_index)
#     return stack

# def correct_non_uniform_lighting(non_uniform_sequence, channel_id, white_estimator=WhiteEstimator(open_size=75, close_size=75, average_size=75)):
#     """ correction of non uniform lighting
#     :param IHyperStack non_uniform_sequence: the input sequence, which is expected to have a non uniform lighting
#     :param WhiteEstimator white_estimator: the method used to estimate the light image
#     :return IHyperStack: the hyperstack of images after correction
#     """
#     white_estimate = white_estimator.estimate_white([non_uniform_sequence], [channel_id])
#     uniform_stack = IImageEngine.get_instance().create_hyperstack(width=white_estimate.get_width(), height=white_estimate.get_height(), num_slices=1, num_frames=non_uniform_sequence.num_frames, num_channels=1, pixel_type=PixelType.F32)

#     for frame_index in range(non_uniform_sequence.num_frames):
#         non_uniform_frame = IImageEngine.get_instance().load_image(non_uniform_sequence.get_image_file_path(channel_index=0, frame_index=frame_index, slice_index=0))
#         uniform_frame = IImageEngine.get_instance().divide(non_uniform_frame, white_estimate)
#         uniform_stack.set_image(frame_index=frame_index, image=uniform_frame)
#     return uniform_stack

def find_white_reference_image(white_estimate, white_z_stack):
    """Find the white reference Image among a z stack of reference image the most correlated to the white image estimated from a serie of acquisition.

    :param IImage white_estimate:
    :param Sequence white_z_stack:
    :param IImageEngine image_engine: the image processor to use

    Based on original matlab code::

        % this function is specific of Telemos images (DISCO line SOLEIL)
            % acquired using micromanager software linked to imageJ

        %% input
        %  nothing : interactive function

        %% output
        %    zmax : focal plane

        %% principe
        % images were acquired for a given roi which position is found in
        % metadata.txt file
        % white and dark  images were recorded for the full field of view of the DISCO
        % TELEMOS camera
        %
        % correlation between a estimated  white fluorescence image estalished from
        % actual acquisitions of a given sample and all z plane acquired for the reference TELEMOS white
        % image (Matthieu slide or any fluorescent homogeneous image) :

        % The  estimated  white fluorescence image  is generallly obtained bt using function whiteFluoImageTelemosestimation
        % This is not compulsary as any homogenous sample image hat can roughly show the shape of illumination can be used to find
        % the white reference image
        %
        %
        % the z plane for which the maximum correlation is observed between estimated white and reference white images is retained.
        % the white image is then offsetted (its corresponding Dark is subtracted) and copied in the subfolder <WhiteReference> of the sample
        % rootfolder to show that it has been specifically selected for the considered experiment
        %The matlab corrcoeff function is used
        %
        % correlation coefficients are saved in a file called
        % 'corr.txt' in the subfolder 'WhiteReference'
        %
        %
        % expected input folder hierarchy:
        %
        %      >sampleFolder
        %           > PosFolders or RoiFolders
        %           > WhiteEstimate
        %       >darkFolder
        %       >darkFolder.smooth
        %       >whiteFolder
        %           > darkFolderforWhite
        %           > darkFolderforWhite.smooth
        %       >whiteFolder.smooth
        %
        %
        % expected output folder hierarchy:
        %
        %      >sampleFolder
        %           > PosFolders or RoiFolders
        %           > WhiteEstimate
        %           > WhiteReference
        %                   > white after offset
        %       >darkFolder
        %       >darkFolder.smooth
        %       >whiteFolder
        %           > darkFolderforWhite
        %           > darkFolderforWhite.smooth
        %       >whiteFolder.smooth





        %% use
        % [zmax]=findWhiteReferenceImageTelemos

        %% Comments
        %   written for  proposal 20161050
        %   adapted for proposal 20170043


        %% Author
        % MF Devaux
        % INRA BIA
        % PVPP

        %% date
        % 5 octobre 2017:
        % 15 decembre 2017 : adapted to take into account the roi known from
        % metadata
        % 27 fevrier 2018 : comments details
        % 4 septembre 2018: comments and check and naming of folders
        % 12 mars 2019 : save white reference with the same size as white estimate
        % 16 avril 2019 : include diagonals to check the relevance of white
        % reference
        % 20 mai 2019 : track folder
        % 27 mai 2019 : offset dark
        %     4 juin 2019 : replace exist by isfolder or isfile
    """
    raise NotImplementedError()

