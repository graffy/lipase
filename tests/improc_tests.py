"""This script is supposed to be launched from fiji's jython interpreter
"""
# # note: fiji's jython doesn't support encoding keyword

# https://imagej.net/Scripting_Headless
#@ String tests_output_data_path

import unittest  # unittest2 doesn't exist in fiji
import sys
from lipase.imageengine import IImageEngine, PixelType, Aabb, NullDebugger, FileBasedDebugger, StackImageFeeder
from lipase.imagej.ijimageengine import IJImageEngine, IJImage
from lipase.circsymdetector import create_circle_image
from lipase import logger

class ImProcTester(unittest.TestCase):

    # we need to know if the test succeeded or not https://stackoverflow.com/questions/4414234/getting-pythons-unittest-results-in-a-teardown-method
    # CURRENT_RESULT = None  # holds last result object passed to run method
    TESTS_OUTPUT_DATA_PATH = tests_output_data_path  # eg '/tmp/lipase/tests-output-data' pylint: disable=undefined-variable

    def setUp(self):
        IImageEngine.set_instance(IJImageEngine(debugger=FileBasedDebugger('%s/debug-images' % self.TESTS_OUTPUT_DATA_PATH)))

    def tearDown(self):
        pass

    def test_create_circle_image(self):
        logger.info("executing test_create_circle_image")
        image_width = 21
        image_height = 17
        circle_image = create_circle_image(
            image_size={'width': 21, 'height': 17},
            circle_radius=7.0,
            circle_pos={'x': 5.0, 'y': 13.0},
            circle_thickness=1.0)
        ie = IImageEngine.get_instance()
        ie.debugger.on_image(circle_image, 'circle')
        measured_mean_value = circle_image.get_mean_value()
        expected_number_of_circle_pixels = 19
        expected_mean_value = float(expected_number_of_circle_pixels) / (image_width * image_height)
        logger.info("expected_mean_value: %f" % expected_mean_value)
        logger.info("measured_mean_value: %f" % measured_mean_value)
        self.assertAlmostEqual(measured_mean_value, expected_mean_value, delta=0.01)

    def test_stack_mean(self):
        ie = IImageEngine.get_instance()
        num_channels = 4
        width = 16
        height = 16
        offset_value = 42.0
        stack = ie.create_hyperstack(width=width, height=height, num_channels=num_channels, num_slices=1, num_frames=1, pixel_type=PixelType.F32)
        for channel_index in range(num_channels):
            im = ie.create_image(width=width, height=height, pixel_type=PixelType.F32)
            im.set_pixel(x=3, y=4, value=channel_index + offset_value)
            stack.set_image(im, channel_index=channel_index)
        mean_image = ie.compute_mean(StackImageFeeder(stack))
        measured_avg = mean_image.get_mean_value()
        sum_of_numbers = num_channels*(num_channels-1)/2 # sum of numbers from 0 to n = n * (n-1) / 2
        expected_avg = (offset_value + float(sum_of_numbers)/num_channels)/(width*height)  
        self.assertAlmostEqual(measured_avg, expected_avg, delta=0.0001)

def run_script():
    logger.debug("executing run_script")

    # unittest.main() # this would result in : ImportError: No module named __main__
    # solution from : https://discourse.mcneel.com/t/using-unittest-in-rhino-python-not-possible/15364
    suite = unittest.TestLoader().loadTestsFromTestCase(ImProcTester)
    stream = sys.stdout  # by default it's sys.stderr, which doesn't appear in imagej's output
    test_result = unittest.TextTestRunner(stream=stream, verbosity=2).run(suite)
    logger.info('test_result : %s' % test_result)
    # store summary of the result in a file so that the caller of imagej can detect that this python script failed (imagej seems to always return error code 0, regardless the error returned by the python script it executes : even sys.exit(1) doesn't change this)
    with open('/tmp/test_result.txt', 'w') as f:
        f.write('%d' % {True: 0, False: 1}[test_result.wasSuccessful()])
    logger.debug('end of run_script')

# note : when launched from fiji, __name__ doesn't have the value "__main__", as when launched from python
run_script()
