.. lipase documentation master file, created by
   sphinx-quickstart on Tue Apr 14 10:32:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lipase's documentation!
==================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   imageengine
   catalog
   lipase
   circsymdetector
   traps_detector
   template_matcher
   maxima_finder
   hdf5
   telemos


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
