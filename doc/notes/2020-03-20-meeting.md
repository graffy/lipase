Réunion d'avancement du 20/03/2020
==================================

Présents
--------
- Véronique Vié
- Guillaume Raffy

Compte-rendu
------------

todo :
- corriger les bug gestion des répertoires windows
- faire en sorte que l'utilisateur puisse lancer une analyse globale de particules
- faire une traitement qui détecte les particules individuelles
- ajouter un numéro de version au package

- pour le flood fill, prendre un pixel du coin plutôt qu'un pixel fourni par l'utilisateur
- ajouter une interface utilisateur pour le match template
- ajouter une interface utilisateur pour le nettoyage d'un piège
- nettoyage du fond (estimate white, mais pour les images visibles)
- ajouter une interface utilisateur pour que l'utilisateur puisse tester correct_non_uniform_lighting