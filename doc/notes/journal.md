## 12/03/2020

- graffy : investigations on how to compute white for sequences containing traps:
    - `telemos.WhiteEstimator` is not suitable for a sequence containing traps, as the resulting white image displays. `telemos.WhiteEstimator` is expecting the input images to only contain small particles.
    - supposing that particles move in the sequences containing trap, the white image can be computed by 


## 17/03/2020

- graffy
    - I made some manual tests for extracting background of `res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0`. For this sequence, the median image provides a worse background image than the last frame of the sequence, which contains nearly no particle. So for now we'll use the last frame as a background for the moment, but we have to find a better background estimator than just the median image (an idea is for example to keep for each pixel the value that is the most frequent in the sequence)
    - Some manual tests on the sequence `res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0` showed that subtracting the background image makes particles more obviusly visible, as expected. Applying a threshold allows to isolate the particles, but estimating the surface ofd particles from that will be tricky, as only the border of particles have a value which is very different from the background. Of course a fill hole operator could fill rthe particles but this can be fragile as the borders of particles are not always clearly visible on all its surrounding.
    - a more promising solution for detecting particles would be to compute for each pixel :
    	- the radial profile around the pixel
    	- the radial variance profile around the pixel
    	then, particle are the pixels for which
    		- the radial variance is low (this means that there's a radial symmetry around the pixel)
    		- and the radial profile shows a peak. The position of the peak gives the radius of the particle
    	- In order to efficiently compute these radial profile and radial variance profiles on a whole image, convolutions methods can provide dramatic speedups. This technique has already been used in https://subversion.ipr.univ-rennes1.fr/repos/main/projects/antipode (`texori.py`). Unfortunately, I can't reuse this code as it makes extensive used of `numpy` (and imagej's jython doesn't support `numpy`)
