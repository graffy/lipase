Réunion d'avancement du 19/06/2018
==================================

Présents
--------
- Véronique Vié
- Guillaume Raffy

Compte-rendu
------------
- analyse de la publi devaux2018
	- A black camera image was taken as the background, and the illumination was estimated from the enzyme image using a low-pass Gaussian filtering of the Fourier transform of the image
	- 	-> pas clair	
- dark
	- objectif x100
		- 40 images de 60s avec chacun des 3 filtres
		- 80 images de 30s avec chacun des 3 filtres
	`img_<temps>_DM300_<filtre>_<signal>_<prof>.tif`
		- filtre :
			- 327-353 : nm
			- 420-480 : nm
		- signal:
			- fluo
			- vis
		- temps : exemple (000000021)
			- numero sur 9 digits
		- profondeur: exemple 003
			- numero sur 3 digits
- échantillons:
	- GGB : globule gras bovin
	- ZeissObjectiveTurret-Label : 2-Ultrafluar 40x/0.60 Glyc
		- objectif à huile : glycerol
		- ouverture 0.60
- on a regardé
	- GGB_cin15_phaseG-I-No-enz_327-vis-352_40x_1
		- traitements à faire:
			- estimer la surface des blobs au cours du temps
			- estimer le niveau de gris moyen des blobs au cours du temps
