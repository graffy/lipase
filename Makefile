SHELL:=/bin/bash 
FIJI_ROOT_PATH:=~/soft/Fiji.app
FIJI_EXE_PATH=$(FIJI_ROOT_PATH)/ImageJ-linux64
# RAW_IMAGES_ROOT_PATH:='/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase/raw-images'
RAW_IMAGES_ROOT_PATH:=$(shell echo ~/work/lipase/raw-images)
TEMP_PATH:=$(shell echo ~/work/lipase/tmp)
TESTS_OUTPUT_DATA_PATH:=$(TEMP_PATH)
LIB_SRC_FILES=$(shell find ./src/lipase -name "*.py")
PLUGINS_SRC_FILES=$(shell find ./src/ij-plugins -name "*.py")
LIPASE_VERSION=1.11

BUILD_ROOT_PATH:=$(TEMP_PATH)/build
PACKAGE_FILE_PATH=$(BUILD_ROOT_PATH)/lipase-$(LIPASE_VERSION).zip

.PHONY: all
all: test doc

.PHONY: doc
doc:
	cd doc; make 

.PHONY: clean_doc
clean_doc:
	cd doc; make clean


$(BUILD_ROOT_PATH)/ij_zip_source/jars/Lib/fr.univ-rennes1.ipr.lipase.lib.jar: $(LIB_SRC_FILES)
	pushd ./src; \
		mkdir -p $(BUILD_ROOT_PATH)/ij_zip_source/jars/Lib ; \
		find ./lipase -name "*.py" > /tmp/files.txt ; \
        jar cvf $(BUILD_ROOT_PATH)/ij_zip_source/jars/Lib/fr.univ-rennes1.ipr.lipase.lib.jar @/tmp/files.txt ; \
		popd

.PHONY: install_ij_opencv
install_ij_opencv:
   # enable IJ-OpenCV-plugins site because it's disabled by default

    # graffy@graffy-ws2:~$ zcat /home/graffy/soft/Fiji.app/db.xml.gz | grep IJ-Op | grep -v "jar" | grep -v "<plu"
    #         <disabled-update-site name="IJ-OpenCV-plugins" url="https://sites.imagej.net/IJ-OpenCV/" official="true" description="Collection of plugins created using the IJ-OpenCV library -- a library that allows the connection of ImageJ and OpenCV." timestamp="20190819101544"/>
    # graffy@graffy-ws2:~$ history | less
    # graffy@graffy-ws2:~$ ~/soft/Fiji.app/ImageJ-linux64 --update edit-update-site IJ-OpenCV-plugins  https://sites.imagej.net/IJ-OpenCV/
    # Java HotSpot(TM) 64-Bit Server VM warning: ignoring option PermSize=128m; support was removed in 8.0
    # Java HotSpot(TM) 64-Bit Server VM warning: Using incremental CMS is deprecated and will likely be removed in a future release
    # Done: Checksummer    
    # graffy@graffy-ws2:~$ zcat /home/graffy/soft/Fiji.app/db.xml.gz | grep IJ-Op | grep -v "jar" | grep -v "<plu"
    #         <update-site name="IJ-OpenCV-plugins" url="https://sites.imagej.net/IJ-OpenCV/" official="true" description="Collection of plugins created using the IJ-OpenCV library -- a library that allows the connection of ImageJ and OpenCV." timestamp="20190819101544"/>
    # $FIJI_ROOT_PATH/ImageJ-linux64 --update edit-update-site IJ-OpenCV-plugins  https://sites.imagej.net/IJ-OpenCV/

    # then apply an update to download and install IJ-OpenCV-plugins :
    # jars/IJ-OpenCV.jar
    # jars/javacv.far
    # jars/opencv-linux-x86.jar 
    # jars/opencv-linux-x86_64.jar 
    # jars/opencv-macosx-x86_64.jar 
    # jars/opencv-windows-x86.jar 
    # jars/opencv-windows-x86_64.jar 
    # jars/opencv.jar 
    # lib/haarcascade_frontalface_alt.xml 

    # graffy@graffy-ws2:~$ ~/soft/Fiji.app/ImageJ-linux64 --update update
    # Java HotSpot(TM) 64-Bit Server VM warning: ignoring option PermSize=128m; support was removed in 8.0
    # Java HotSpot(TM) 64-Bit Server VM warning: Using incremental CMS is deprecated and will likely be removed in a future release
    # Done: Checksummer
    # Done: Downloading...
    # Done: Downloading...
    # $FIJI_ROOT_PATH/ImageJ-linux64 --update update

	ls $(FIJI_ROOT_PATH)/jars/IJ-OpenCV-*.jar ; \
		if [ $$? != 0 ] ; \
		then \
			$(FIJI_ROOT_PATH)/ImageJ-linux64 --update edit-update-site IJ-OpenCV-plugins  https://sites.imagej.net/IJ-OpenCV/; \
			$(FIJI_ROOT_PATH)/ImageJ-linux64 --update update; \
		fi

$(PACKAGE_FILE_PATH): $(BUILD_ROOT_PATH)/ij_zip_source/jars/Lib/fr.univ-rennes1.ipr.lipase.lib.jar $(PLUGINS_SRC_FILES)
	mkdir -p $(BUILD_ROOT_PATH)/ij_zip_source/plugins/Ipr ; \
		rsync -a ./src/ij-plugins/Ipr/ $(BUILD_ROOT_PATH)/ij_zip_source/plugins/Ipr/ ; \
        pushd $(BUILD_ROOT_PATH)/ij_zip_source ; \
		zip -r $(PACKAGE_FILE_PATH) * ; \
        popd

.PHONY: package
package: $(PACKAGE_FILE_PATH)

.PHONY: install
install: $(PACKAGE_FILE_PATH) install_ij_opencv
	pushd $(FIJI_ROOT_PATH); unzip -o $(PACKAGE_FILE_PATH); popd

.PHONY: test0001
test0001: install
	# on macosx : /Applications/Fiji.app/Contents/MacOS/ImageJ-macosx --ij2 --headless --run './test0001.py'
	# /Applications/Fiji.app/Contents/MacOS/ImageJ-macosx --ij2 --headless --run './tests/test0001.py' "lipase_src_root_path='$(pwd)',raw_images_root_path='/Users/graffy/ownCloud/ipr/lipase/raw-images'"
	echo 2 > '/tmp/test_result.txt' ; \
		mkdir -p $(TESTS_OUTPUT_DATA_PATH)
	 	$(FIJI_EXE_PATH) --ij2 --headless --run './tests/test0001.py' "raw_images_root_path='$(RAW_IMAGES_ROOT_PATH)',tests_output_data_path='$(TESTS_OUTPUT_DATA_PATH)'" ; \
		ERROR_CODE=$$? ; \
		echo "Fiji 's return code : $$ERROR_CODE" ; \
		ERROR_CODE=$$(cat '/tmp/test_result.txt') ; \
		echo "test's return code : $$ERROR_CODE" ; \
		exit $$ERROR_CODE

.PHONY: test0001
imgproc_tests: install
	echo 2 > '/tmp/test_result.txt' ; \
	 	$(FIJI_EXE_PATH) --ij2 --headless --run './tests/improc_tests.py' "tests_output_data_path='$(TESTS_OUTPUT_DATA_PATH)'" ; \
		ERROR_CODE=$$? ; \
		echo "Fiji 's return code : $$ERROR_CODE" ; \
		ERROR_CODE=$$(cat '/tmp/test_result.txt') ; \
		echo "test's return code : $$ERROR_CODE" ; \
		exit $$ERROR_CODE


.PHONY: test_globules_area
test_globules_area: install
	# on macosx : /Applications/Fiji.app/Contents/MacOS/ImageJ-macosx --ij2 --headless --run './test0001.py'
	# /Applications/Fiji.app/Contents/MacOS/ImageJ-macosx --ij2 --headless --run './tests/test0001.py' "lipase_src_root_path='$(pwd)',raw_images_root_path='/Users/graffy/ownCloud/ipr/lipase/raw-images'"
	echo 2 > '/tmp/test_result.txt' ; \
	 	$(FIJI_EXE_PATH) --ij2 --headless --run './src/ij-plugins/Ipr/Lipase/Compute_Globules_Area.py' "INPUT_STACK='$(RAW_IMAGES_ROOT_PATH)/res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0/img_000000000_DM300_nofilter_vis_000.tif', INPUT_BACKGROUND='$(RAW_IMAGES_ROOT_PATH)/res_soleil2018/GGH/GGH_2018_cin2_phiG_I_327_vis_-40_1/Pos0/img_000000000_DM300_nofilter_vis_000.tif', PARTICLE_THRESHOLD='2000'" ; \
		ERROR_CODE=$$? ; \
		echo "Fiji 's return code : $$ERROR_CODE" ; \
		ERROR_CODE=$$(cat '/tmp/test_result.txt') ; \
		echo "test's return code : $$ERROR_CODE" ; \
		exit $$ERROR_CODE

.PHONY: test
test: test0001 imgproc_tests

.PHONY: clean
clean: clean_doc
	rm -Rf $(BUILD_ROOT_PATH)
