#!/bin/bash
DOWNLOAD_FIJI_LATEST_VERSION='false'
if [ "$DOWNLOAD_FIJI_LATEST_VERSION" = 'true' ]
then
    wget 'https://downloads.imagej.net/fiji/latest/fiji-linux64.zip'
    unzip './fiji-linux64.zip'
    rm './fiji-linux64.zip'
else
    # use the version downloaded with :
    # graffy@physix-frontal:/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase$ wget 'https://downloads.imagej.net/fiji/latest/fiji-linux64.zip'
    # graffy@physix-frontal:/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase$ mv fiji-linux64.zip ./fiji-linux64-2019-07-02.zip
    # note : the date of the package is visible on https://downloads.imagej.net/fiji/latest/
    FIJI_PACKAGE_FILE_PATH='/opt/ipr/cluster/work.global/graffy/jenkins-store/lipase/fiji-linux64-2019-07-02.zip'
    echo "using pre-downloaded fiji package $FIJI_PACKAGE_FILE_PATH"
    unzip "$FIJI_PACKAGE_FILE_PATH"
fi
