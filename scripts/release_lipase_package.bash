#!/bin/bash
# LIPASE_PACKAGE_FILE_PATH=$(ls build/lipase*.zip | tail -1)  # tail is not guaranteed to be the latest version once we move from vertion 9 to version 10
LIPASE_VERSION=$(cat ./Makefile | awk -F= '/^LIPASE_VERSION/ {print $2}')
LIPASE_PACKAGE_FILE_PATH="build/lipase-${LIPASE_VERSION}.zip"
LIPASE_USER_MANUAL_PATH=$(pwd)/doc/lipase.pdf
echo "LIPASE_PACKAGE_FILE_PATH=$LIPASE_PACKAGE_FILE_PATH"
LIPASE_PACKAGE_FILE_NAME=$(basename $LIPASE_PACKAGE_FILE_PATH)
if [ "$GRAFFY_OWNCLOUD_JENKINS_PWD" = '' ]
then
    echo "the value of the environment variable GRAFFY_OWNCLOUD_JENKINS_PWD is empty, while its contents are expected to come from JenkinsFile (this script is designed to be called from the accompanying JenkinsFile)"
    exit 1
fi

virtualenv ./python.virtualenv
source ./python.virtualenv/bin/activate
pip install pyocclient  # installs python owncloud module

RELEASE_DIR='lipase'  # the lipase owncloud directory has been created by gpaboeuf on 08/07/2020, is owned by iprbiomif, and has been shared with graffy, with write access
python -c "import owncloud; oc = owncloud.Client('https://cloud.ipr.univ-rennes1.fr'); oc.login('graffy','$GRAFFY_OWNCLOUD_JENKINS_PWD'); oc.put_file('$RELEASE_DIR/$LIPASE_PACKAGE_FILE_NAME', r'$LIPASE_PACKAGE_FILE_PATH'); oc.put_file('$RELEASE_DIR/lipase-manual.pdf', r'$LIPASE_USER_MANUAL_PATH');"

